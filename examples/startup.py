#from pathlib import Path
from pymid.aiT.AiTWCETInterface import AiTWCETInterface
# from pymid.dummyWCETSolver.Solver import dummyWCETSolver

BASE_DIR = "/home/nbellec/research/projectM2/m2_proj"
DATA_DIR = "/home/nbellec/research/projectM2/m2_proj/experiments"
RESULTS_DIR = DATA_DIR + "/results"
AIS_DIR = BASE_DIR + "/benchmarks/build/heptane_benchmarks/ais"
PROG_NAME = "fft"

exe = "{}.exe".format( PROG_NAME )
ais_file = "{}.ais".format( PROG_NAME )
ais = AIS_DIR + ais_file
starting_point = "main"
program_results_dir = RESULTS_DIR + "/%s" % PROG_NAME
program_dir = BASE_DIR + "/benchmarks/build/heptane_benchmarks"
global_results_dir = RESULTS_DIR
tmp_dir = DATA_DIR + "/.tmp"
max_subSese = None
max_stack = None
resulting_data_file = "{}.data".format( PROG_NAME )


startup = {
	"objdump": "/opt/bcc-2.0.7-gcc/bin/sparc-gaisler-elf-objdump",
	"program_dir": program_dir,
	"programs": {
		exe: {
			"ais": ais,
			"results_dir": program_results_dir,
			"starting_point": starting_point
		}
	},
	"memory_space": None,
	"stack_space": max_stack,
	"max_subSese": max_subSese,
	"options": [],
	"results_dir": global_results_dir,
	"resulting_data_file": resulting_data_file,
	"tmp_dir": tmp_dir,
	"WCETSolverInterface": AiTWCETInterface,
}
