#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from pathlib import Path
import sys

from pymid.aiT.aiTProject import aiTProject
from pymid.aiT.aiTAnalyses import aiTWcetAnalysis
from pymid.aiT.reader import AiTXmlReader
from pymid.aiT.ais import AisFile

def main():
    if len(sys.argv) < 4:
        print("./auto_ais binary new_ais starting_point [base_ais]")
        exit(-1)

    binary = Path(sys.argv[1])
    ais = Path(sys.argv[2])
    starting_point = Path(sys.argv[3])
    base_ais = Path(sys.argv[4]) if len(sys.argv) == 5 else None

    if not binary.exists():
        print("binary %s does not exists" % str(binary))
        exit(-2)

    aiT = aiTProject( "aisGen", Path("./auto_ais") )
    aiT.apx.add_executable( binary )
    analysisName = "loopBound"
    analysis = aiTWcetAnalysis( analysisName )
    analysis.start = starting_point

    aiT.add_analysis( analysis )

    analysis.xmlReport = "%s.xml" % analysisName

    if base_ais is not None:
        base_ais = AisFile( base_ais.parent, base_ais.stem, to_write=False )
        analysis.add_ais( base_ais )

    aiT.run()

    xmlResult = AiTXmlReader.read_xml( analysis.get_report_path( 'xml' ) )
    xmlResult.loop_bound_analysis()

    with open(str(ais), "w") as f:
        for r_name, r_res in xmlResult.results['loop_bound_analysis'].items():
            r_min, r_max = r_res
            f.write("loop \"%s\" { bound: %d .. %d; }\n" % (r_name, r_min, r_max))


if __name__ == "__main__":
    main()
