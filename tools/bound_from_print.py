# -*- coding: utf-8 -*-

import sys
from pathlib import Path
import re

def get_function_name( line_name : str ) :
    if " : end" in line_name:
        return line_name[:-6]
    if " : start" in line_name:
        return line_name[ :-8 ]
    return line_name

if len( sys.argv ) < 2:
    print( "Missing program output" )
    exit( -1 )

program_output = Path( sys.argv[1] )

if not program_output.exists():
    print( "%s does not exists" % str( program_output ) )

stack = []
cur_counters = dict()
final_counters = dict()
# final_num = dict()

with program_output.open() as output_file:
    for number, output_line in enumerate(output_file):
        strip_line = output_line.strip()
        func_name = get_function_name( strip_line )

        if func_name == strip_line:
            if strip_line in stack:
                # l already has a cur_counters to update

                while strip_line != stack[ -1 ]:
                    # We need to pop the stack and update the final_counters accordingly
                    finished_loop = stack.pop()
                    previous_max = final_counters.get( finished_loop, 0 )
                    if previous_max < cur_counters[ finished_loop ]:
                        final_counters[ finished_loop ] = cur_counters[ finished_loop ]
                        # final_num[ finished_loop ] = number

                    cur_counters.__delitem__( finished_loop )

                cur_counters[ strip_line ] += 1
            else:
                stack.append( strip_line )
                cur_counters[ strip_line ] = 1
        else:
            if " : start" in strip_line:
                stack.append( func_name )

            if " : end" in strip_line:
                while func_name != stack[ -1 ]:
                    finished_loop = stack.pop()
                    previous_max = final_counters.get( finished_loop, 0 )
                    if previous_max < cur_counters[ finished_loop ] :
                        final_counters[ finished_loop ] = cur_counters[ finished_loop ]

                # Pop the function
                stack.pop()

# We pop the whole stack
while len( stack ) > 0:
    # We need to pop the stack and update the final_counters accordingly
    finished_loop = stack.pop()
    previous_max = final_counters.get( finished_loop, 0 )
    if previous_max < cur_counters[ finished_loop ] :
        final_counters[ finished_loop ] = cur_counters[ finished_loop ]
        # final_num[ finished_loop ] = "last"

accepted_loop_name = "[a-zA-Z]+[.]L[0-9]+"

for loop_name, max_iter in final_counters.items():
    if re.search( accepted_loop_name, loop_name) is not None:
        print("loop \"%s\" { bound: 0 .. %d; } " % (loop_name, max_iter))
