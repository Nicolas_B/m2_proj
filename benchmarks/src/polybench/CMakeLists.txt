set(POLYBENCH_DATAMINING
# correlation # math lib use
covariance
)

set( PREFIX "poly" )
SET( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DSMALL_DATASET -DPOLYBENCH_STACK_ARRAYS -DPOLYBENCH_NO_FLUSH_CACHE -DDATA_TYPE_IS_DOUBLE"  )


foreach(BENCH ${POLYBENCH_DATAMINING})
  set(exe "${PREFIX}_${BENCH}.exe")
  add_executable("${exe}" "datamining/${BENCH}/${BENCH}.c" "utilities/polybench.c")
  target_include_directories(
    "${exe}" PUBLIC
    "datamining/${BENCH}"
    "includes"
    "utilities"
  )
endforeach()

set(POLYBENCH_LINALG_BLAS
gemm
gemver
gesummv
symm
syr2k
syrk
trmm
)

foreach(BENCH ${POLYBENCH_LINALG_BLAS})
  set(exe "${PREFIX}_${BENCH}.exe")
  add_executable("${exe}" "linear-algebra/blas/${BENCH}/${BENCH}.c" "utilities/polybench.c")
  target_include_directories(
    "${exe}" PUBLIC
    "linear-algebra/blas/${BENCH}"
    "includes"
    "utilities"
  )
endforeach()

set(POLYBENCH_LINALG_KERNELS
2mm
3mm
atax
bicg
doitgen
mvt
)

foreach(BENCH ${POLYBENCH_LINALG_KERNELS})
  set(exe "${PREFIX}_${BENCH}.exe")
  add_executable("${exe}" "linear-algebra/kernels/${BENCH}/${BENCH}.c" "utilities/polybench.c")
  target_include_directories(
    "${exe}" PUBLIC
    "linear-algebra/kernels/${BENCH}"
    "includes"
    "utilities"
  )
endforeach()

set(POLYBENCH_LINALG_SOLVERS
# cholesky # math lib use
durbin
# gramschmidt # math lib use
lu
ludcmp
trisolv
)

foreach(BENCH ${POLYBENCH_LINALG_SOLVERS})
  set(exe "${PREFIX}_${BENCH}.exe")
  add_executable("${exe}" "linear-algebra/solvers/${BENCH}/${BENCH}.c" "utilities/polybench.c")
  target_include_directories(
    "${exe}" PUBLIC
    "linear-algebra/solvers/${BENCH}"
    "includes"
    "utilities"
  )
endforeach()

set(POLYBENCH_MEDLEY
# deriche # math lib use
floyd-warshall
nussinov
)

foreach(BENCH ${POLYBENCH_MEDLEY})
  set(exe "${PREFIX}_${BENCH}.exe")
  add_executable("${exe}" "medley/${BENCH}/${BENCH}.c" "utilities/polybench.c")
  target_include_directories(
    "${exe}" PUBLIC
    "medley/${BENCH}"
    "includes"
    "utilities"
  )
endforeach()


set(POLYBENCH_STENCILS
adi
fdtd-2d
heat-3d
jacobi-1d
jacobi-2d
seidel-2d
)

foreach(BENCH ${POLYBENCH_STENCILS})
  set(exe "${PREFIX}_${BENCH}.exe")
  add_executable("${exe}" "stencils/${BENCH}/${BENCH}.c" "utilities/polybench.c")
  target_include_directories(
    "${exe}" PUBLIC
    "stencils/${BENCH}"
    "includes"
    "utilities"
  )
endforeach()
