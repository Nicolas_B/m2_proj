#ifdef AIS_GEN_ON
#include <stdio.h>

#define AIS_GEN_START_FUNC(FUNC) printf ( #FUNC " : start\n");
#define AIS_GEN_END_FUNC(FUNC) printf ( #FUNC " : end\n");
#define AIS_GEN_LOOP(FUNC, LOOP) printf ( #FUNC "." #LOOP "\n");

#else

#define AIS_GEN_START_FUNC(FUNC)
#define AIS_GEN_END_FUNC(FUNC)
#define AIS_GEN_LOOP(FUNC, LOOP)


#endif
