#!/bin/bash

top_directory=$(pwd)
script_directory="${top_directory}"

leon3_dir="/udd/nbellec/Documents/grlib-gpl-2019.4-b4246/designs/leon3-terasic-de2-115/"

# source "${script_directory}/benchmarks.sh"
source "${script_directory}/settings.sh"

benchmarks=(
"cnt.exe"
"crc.exe"
"fft.exe"
"ludcmp.exe"
"matmult.exe"
"minver.exe"
"edn.exe"
"st.exe"
"qsort-exam.exe"
)

poly_benchmarks=(
"poly_gemver.exe"
"poly_3mm.exe"
"poly_ludcmp.exe"
"poly_covariance.exe"
"poly_nussinov.exe"
"poly_adi.exe"
"poly_fdtd-2d.exe"
"poly_heat-3d.exe"
)

function launch_sim {
  bench=$1
  entry_point=$2

  bench_name=$(basename $bench ".exe")

  srec_dir="${script_directory}/srec/"
  simscript_dir="${script_directory}/simscripts/"

  simscript="simscript_${bench_name}.do"
  srec="${bench_name}.srec"
  out_file="${script_directory}/outputs/${bench_name}.out"
  vcd_file="log_pc_${bench_name}.vcd"
  stored_vcd="${script_directory}/vcd/${vcd_file}"

  cp "${srec_dir}/${srec}" "${leon3_dir}/ram.srec"

  cd "${leon3_dir}"
  vsim -c -quiet -voptargs="+acc -nowarn 1" testbench < "${simscript_dir}/${simscript}" > "${out_file}"
  cp "${vcd_file}" "${stored_vcd}"
}


for bench in "${benchmarks[@]}"
do
  # echo ${bench}
  launch_sim ${bench}
done
