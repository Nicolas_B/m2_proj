#!/bin/bash

top_directory=$(pwd)
script_directory="${top_directory}/scripts"

source "${script_directory}/benchmarks.sh"

grlib_diretory="/home/nbellec/udd/nbellec/Documents/grlib-gpl-2019.4-b4246/designs/leon3-terasic-de2-115/"
fetch_pc_path="sim:/testbench/d3/cpu(0)/leon3/vhdl/p0/cpi.e.pc"
clock_path="sim:/testbench/d3/cpu(0)/leon3/vhdl/p0/iu/clk"
bpmiss_path="sim:/testbench/d3/cpu(0)/leon3/vhdl/p0/iu/dbgo.bpmiss"

benchmarks=(
"polybench"
"mdh"
"heptane_benchmarks"
)

function pre_sim {
  bench=$1
  entry_point=$2

  bench_name=$(basename $bench ".exe")
  program_dir=$(dirname $bench)

  srec_dir="${program_dir}/srec/"
  simscript_dir="${program_dir}/simscripts/"

  simscript="simscript_${bench_name}.do"
  expectscript="interact_${bench_name}.do"

  mkdir -p ${srec_dir}
  mkdir -p ${simscript_dir}

  simscript_file="${simscript_dir}/${simscript}"
  expectscript_file="${simscript_dir}/${expectscript}"

  sparc-gaisler-elf-objcopy -O srec --gap-fill 0 ${bench} "${srec_dir}/${bench_name}.srec"

  # Generate the script of simulation
  main_add=`sparc-gaisler-elf-objdump -x ${bench} | grep "${entry_point}" | awk -F" " '{print \$1}' | tr "[a-z]" "[A-Z]"`
  #fetch_main_add=`echo "obase=16;ibase=16;${main_add}/4" | bc`

  # Insert the breakpoint on main
  echo "when -label main_bp { ${fetch_pc_path} = 16#${main_add} } { echo { Main breakpoint }; stop }" > ${simscript_file}
  echo "run -all" >> ${simscript_file}
  echo "nowhen main_bp" >> ${simscript_file}
  echo "vcd file log_pc_${bench_name}.vcd" >> ${simscript_file}
  echo "vcd add ${fetch_pc_path}" >> ${simscript_file}
  echo "vcd add ${clock_path}" >> ${simscript_file}
  echo "vcd add ${clock_path}" >> ${simscript_file}
  echo "vcd add ${bpmiss_path}" >> ${simscript_file}
  echo "vcd on" >> ${simscript_file}
  echo "run -all" >> ${simscript_file}
  echo "quit" >> ${simscript_file}

  # Generate the script of interaction to launch the simulation
  echo "#!/usr/bin/expect -f" > ${expectscript_file}
  echo "set timeout -1" >> ${expectscript_file}
  echo "spawn ssh cairn-cao1" >> ${expectscript_file}
  echo "expect -gl \"-bash-4.2\"" >> ${expectscript_file}
  echo "send -- \"cd Documents\r\"" >> ${expectscript_file}
  echo "send -- \". settings.sh\r\"" >> ${expectscript_file}
  echo "send -- \"cd grlib-gpl-2019.4-b4246/designs/leon3-terasic-de2-115\r\"" >> ${expectscript_file}
  echo "send -- \"vsim -c -quiet -voptargs=\\\"+acc -nowarn 1\\\" testbench < ${simscript}\r\"" >> ${expectscript_file}
  echo "expect \"# End time:\"" >> ${expectscript_file}
  echo "send -- \"exit\r\"" >> ${expectscript_file}
  chmod +x ${expectscript_file}
}

function launch_sim {
  bench=$1

  bench_name=$(basename $bench ".exe")
  program_dir=$(dirname $bench)

  srec_dir="${program_dir}/srec"
  simscript_dir="${program_dir}/simscripts"

  simscript="simscript_${bench_name}.do"
  simscript_file="${simscript_dir}/${simscript}"

  expectscript="interact_${bench_name}.do"
  expectscript_file="${simscript_dir}/${expectscript}"

  scp ${simscript_file} "cairn-cao1:/udd/nbellec/Documents/modelsim_exp/simscripts/${simscript}"
  scp "${srec_dir}/${bench_name}.srec" "cairn-cao1:/udd/nbellec/Documents/modelsim_exp/srec/${bench_name}.srec"

  #"./${expectscript_file}"
}

function post_sim {
  bench=$1

  bench_name=$(basename $bench ".exe")
  program_dir=$(dirname $bench)

  vcd_filename="log_pc_${bench_name}.vcd"
  vcd_filepath="/udd/nbellec//Documents/grlib-gpl-2019.4-b4246/designs/leon3-terasic-de2-115/${vcd_filename}"

  experimental_dir="/home/nbellec/research/projectM2/simulation_results"

  mkdir -p "${experimental_dir}"

  scp "cairn-cao1:${vcd_filepath}" "${experimental_dir}/${vcd_filename}"
}

# bench_test="benchmarks/build/heptane_benchmarks/bs.exe"

for benchmark_name in ${benchmarks[@]}
do
  declare -n benchmark=${benchmark_name}
  for bench in "${!benchmark[@]}"
  do
    pre_sim ${bench} ${benchmark[${bench}]}
    launch_sim ${bench}
    #post_sim ${bench}
  done
done
