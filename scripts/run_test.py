#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import pprint

from analysis.builders.PDSTBuilder import *
from analysis.program import Function
from analysis.CFG import CFG
from utils.graphUtility import printDotPDST
from utils import loggingConf

HUMAN_ANALYSIS = False

logger = loggingConf.getLogger("run_test")

if __name__ == '__main__':
	def print_nodes(graph):
		for n in graph.nodes:
			print(str(n) + '\t:\t' + str(graph.nodes[n]))

	def print_edges(graph):
		for e in graph.edges:
			print(str(e) + '\t:\t' + str(graph.edges[e]))

	def print_dict(d):
		for k in d:
			print('\t'+str(k)+' : '+str(d[k]))

	def analysis_result( graph, sese_tree ):
		print(str(graph.name)+" Analysis")
		print()
		print("  SESE Tree Nodes  ")
		print_nodes( sese_tree.get_graph() )
		print()
		print("  SESE Tree Edges  ")
		print_edges( sese_tree.get_graph() )
		print()
		print("  CFG Nodes  ")
		print_nodes(graph)
		print()
		print("  CFG Edges  ")
		print_edges(graph)
		printDotPDST( graph, sese_tree )

	def pdst_node_match(n1, n2):
		if n1 == n2:
			return True
		else:
			# Deal with the root node
			if n1.get("entry") is None and n1.get( "entry" ) is None :
				return True
			else:
				return False

	def pdst_edge_match(e1, e2):
		return e1 == e2

	def genGraph(GSpe, name=""):
		G = nx.DiGraph(name=name)
		G.add_nodes_from(GSpe.get("nodes", []))
		G.add_edges_from(GSpe.get("edges",[]))

		for n in GSpe.get("node_atts", {}):
			for att in GSpe["node_atts"][n]:
				G.nodes[n][att] = GSpe["node_atts"][n][att]
		for e1,e2 in GSpe.get("edge_atts", {}):
			for att in GSpe["edge_atts"][(e1,e2)]:
				G.edges[e1][e2][att] = GSpe["edge_atts"][(e1,e2)][att]

		return G

	def genTests(TESTS):
		for T in TESTS:
			name = T.get("name", "")
			cfgSpe = T["cfg"]
			pdstSpe = T["pdst"]

			cfg = genGraph(cfgSpe, name)
			pdst = genGraph(pdstSpe, name)

			F = Function(name, 0)
			F.name = name
			F.cfg = CFG(F)
			F.cfg.graph = cfg

			yield (F, pdst)

	builder = PDSTBuilder

	PDSTBuilder_TESTS = [
		{
			"name" : "G0",
			"cfg" : {
				"edges" : [
					('start',0),
					(0,1),
					(1,2),
					(2,3),
					(2,4),
					(0,5),
					(5,'end'),
					(3,'end'),
					(4,'end'),
				]
			},
			"pdst" : {
				"edges" : [
					('root',0),
					('root',1),
					('root',2),
					('root',3),
				],
			},
		},
		{
			"name" : "G1",
			"cfg" : {
				"edges" : [
					('start',0),
					(0,1),
					(0,2),
					(2,3),
					(3,4),
					(3,5),
					(4,6),
					(5,6),
					(1,'end'),
					(6,'end')
				]
			},
			"pdst" : {
				"edges" : [
					('root',0),
					('root',1),
					(1, (1,0)),
					(1, (1,1)),
					((1,1),(3,0)),
					((1,1),(4,0)),
				],
			},
		},
		{
			"name" : "G3",
			"cfg" : {
				"edges" : [
					('start', 0),
					(0,1),
					(1,2),
					(1,3),
					(1,8),
					(2,4),
					(2,5),
					(3,6),
					(3,7),
					(4,8),
					(5,8),
					(6,8),
					(7,8),
					(8, 'end')
				]
			},
			"pdst" : {
				"edges" : [
					('root',0),
					(0, (0,0)),
					(0, (0,1)),
					((0,1),(1,0)),
					((0,1),(2,0)),
					((0,1),(3,0)),
					((0,1),(4,0)),
				],
			},
		},
		{
			"name" : "G",
			"cfg" : {
				"edges" : [
					('start', 0),
					(0,1),
					(0,5),
					(1,2),
					(1,3),
					(2,4),
					(3,4),
					(4,9),
					(9,10),
					(5,6),
					(6,7),
					(6,8),
					(10, 'end'),
					(7, 'end'),
					(8, 'end')
				]
			},
			"pdst" : {
				"edges" : [
					('root',0),
					('root',1),
					('root',2),
					('root',3),
					(3, (3,0)),
					(3, (3,1)),
					(3, (3,2)),
					((3,0), (4,0)),
					((3,0), (5,0)),
				],
			},
		},
	]

	for function, pdst in genTests(PDSTBuilder_TESTS):
		pdstBuilt = builder.build_pdst( function )

		if HUMAN_ANALYSIS:
			analysis_result( function.cfg.get_graph(), pdstBuilt )

		if not nx.is_isomorphic( pdstBuilt.get_graph(), pdst ):
			logger.error("""The PDST of '%s' is incorrect""" % function.name)
			raise Exception()
