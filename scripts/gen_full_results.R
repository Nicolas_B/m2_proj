#!/usr/bin/env Rscript

perform_analyses <- function(data_exp, data_step_by_step) {
  n_bench = length(levels(data_exp$name))
  n_exp = length(levels(data_exp$exp))

  # Latter change needed
  data <- data.frame(
    benchmark=levels(data_exp$name),
    "unbounded"=rep(0, n_bench),
    "4"=rep(0, n_bench),
    "8"=rep(0, n_bench)
  )

  # Iterate on the experiences
  for ( benchname in levels(data_exp$name) ) {
    data_bench = data_step_by_step[data_step_by_step$program==benchname,]
    general_bench = data_exp[data_exp$name==benchname,]

    data[data$benchmark==benchname,]$unbounded <-general_bench[general_bench$exp=="_expanded",]$final.goal
    data[data$benchmark==benchname,]$X4 <- general_bench[general_bench$exp=="_A4",]$final.goal
    data[data$benchmark==benchname,]$X8 <- general_bench[general_bench$exp=="_A8",]$final.goal
  }

  return(data)
}

interesting_informations <- function(analysis_data) {
  n = length(analysis_data$benchmark)

  data <- data.frame(
    benchmark=analysis_data$benchmark,
    ratio_X4_unbounded=rep(0, n),
    ratio_X8_unbounded=rep(0, n),
    unbounded=analysis_data$unbounded,
    X4=analysis_data$X4
    # X8=analysis_data$X8
  )

  for ( bench in analysis_data$benchmark ) {
    data[data$benchmark==bench,]$ratio_X4_unbounded = analysis_data[analysis_data$benchmark==bench,]$X4 / analysis_data[analysis_data$benchmark==bench,]$unbounded
    data[data$benchmark==bench,]$ratio_X8_unbounded = analysis_data[analysis_data$benchmark==bench,]$X8 / analysis_data[analysis_data$benchmark==bench,]$unbounded
  }

  # print(analysis_data[analysis_data$benchmark != "nsichneu",]$X4)
  print( data )
  print( length(data[data$ratio_X4_unbounded == 1,]$benchmark) )
  print( length(data[data$ratio_X4_unbounded < 2,]$benchmark) )
  print( length(data[data$ratio_X8_unbounded == 1,]$benchmark) )
  print( length(data[data$ratio_X8_unbounded < 2,]$benchmark) )

  # print(sd(analysis_data[analysis_data$benchmark != "nsichneu",]$X4 / analysis_data[analysis_data$benchmark != "nsichneu",]$unbounded))
  # print(mean(analysis_data$X8 / analysis_data$unbounded))
  # print(sd(analysis_data$X8 / analysis_data$unbounded))
}

args = commandArgs(trailingOnly=TRUE)
filename_general_data <- args[1]
filename_aggregated_steps <- args[2]

data_aggregated = read.csv(file=filename_aggregated_steps, sep=";", header=TRUE)
data_general = read.csv(file=filename_general_data, sep=";", header=TRUE)

analysis_data <- perform_analyses(data_general, data_aggregated)

write.table(analysis_data, file="MAW_vs_arity.csv", quote=FALSE, sep=";", row.names=FALSE)

interesting_informations(analysis_data)
