#!/bin/bash

top_directory=$(pwd)
script_directory="${top_directory}/scripts"

source "${script_directory}/benchmarks.sh"

benchmarks=(
"polybench"
"mdh"
"heptane_benchmarks"
)

declare -A experiments_options=(
["_base"]=""
# ["_graph"]="--expand_cfg --graph"
# ["_info"]="--expand_cfg --only-info"
["_expanded"]="--expand_cfg"
["_A4"]="--expand_cfg"
["_A8"]="--expand_cfg"
)

declare -A experiments_max_subSeses=(
["_base"]="None"
# ["_graph"]="None"
# ["_info"]="None"
["_expanded"]="None"
["_A4"]="4"
["_A8"]="8"
)

function auto_gen_ais {
  EXECUTABLE=$1
  AUTO_AIS=$2
  starting_point=$3

  BENCH_DIR=$(basename $(dirname ${EXECUTABLE}))
  EXECUTABLE_NAME=$(basename ${EXECUTABLE} ".exe")

  EXISTING_AIS="benchmarks/ais/${BENCH_DIR}/${EXECUTABLE_NAME}.ais"
  BASE_AIS="benchmarks/ais/${BENCH_DIR}/${EXECUTABLE_NAME}.base.ais"

  mkdir -p $(dirname ${AUTO_AIS})

  touch "${AUTO_AIS}"
  if [ -f "${EXISTING_AIS}" ]; then
    cp ${EXISTING_AIS} ${AUTO_AIS}
  else
    if [ -f "${BASE_AIS}" ]; then
      python3 tools/auto_ais.py ${EXECUTABLE} ${AUTO_AIS} "${starting_point}" ${BASE_AIS}
    else
      python3 tools/auto_ais.py ${EXECUTABLE} ${AUTO_AIS} "${starting_point}"
    fi
    if [ -z "$?" ]
    then
      rm -r "auto_ais/"
    fi
  fi
}

function gen_startup_file {
  bench=$1
	starting_point=$2
  suffix=$3
  max_subSese=$4

  experiment_dir="${top_directory}/experiments"

	bench_name=$(basename $bench ".exe")
	program_dir=$(dirname $bench)
  benchmark_name=$(basename ${program_dir})
	exe=$(basename $bench)
	ais="${program_dir}/ais/auto_${bench_name}.ais"
  startup_file="${program_dir}/startup/${bench_name}${suffix}.py"
  resulting_data_file="${bench_name}.data"


  global_results_dir="${experiment_dir}/results${suffix}"
  program_results_dir="${global_results_dir}/${bench_name}"
	tmp_dir="${experiment_dir}/.tmp${suffix}"

  mkdir -p $(dirname ${startup_file})
	mkdir -p ${experiment_dir}
	mkdir -p ${program_results_dir}
	mkdir -p ${global_results_dir}
	mkdir -p ${tmp_dir}

  auto_gen_ais "${bench}" "${ais}" "${starting_point}"

  sed "/exe =/c\exe = \"${exe}\"" examples/startup.py > ${startup_file}
	sed -i "/ais =/c\ais = \"${ais}\"" ${startup_file}
	sed -i "/starting_point =/c\starting_point = \"${benchmark[${bench}]}\"" ${startup_file}
	sed -i "/program_results_dir =/c\program_results_dir = \"${program_results_dir}\"" ${startup_file}
	sed -i "/program_dir =/c\program_dir = \"${program_dir}\"" ${startup_file}
	sed -i "/global_results_dir =/c\global_results_dir = \"${global_results_dir}\"" ${startup_file}
  sed -i "/resulting_data_file =/c\resulting_data_file = \"${resulting_data_file}\"" ${startup_file}
	sed -i "/tmp_dir =/c\tmp_dir = \"${tmp_dir}\"" ${startup_file}
  sed -i "/max_subSese =/c\max_subSese = ${max_subSese}" "${startup_file}"
}

function gen_bench_level_results {
  suffix=$1
  bench=$2

  experiment_dir="${top_directory}/experiments"
  bench_name=$(basename $bench ".exe")
  program_dir=$(dirname $bench)

  global_results_dir="${experiment_dir}/results${suffix}"
  program_results_dir="${global_results_dir}/${bench_name}"

  iterative_data="${program_results_dir}/step_by_step_mid.csv"
  iterative_rplot_script="${script_directory}/gen_bench_results.R"

  location_resulting_plot="${program_results_dir}/${bench_name}${suffix}_step_by_step_mid_plot_normal.pdf"

  if [ ! -f ${iterative_data} ]
  then
    echo "Bench : ${bench} failed : no step by step MID data"
  else
    Rscript "${iterative_rplot_script}" "${iterative_data}"
    mv "Rplots.pdf" "${location_resulting_plot}"
  fi
}

function gen_exp_level_results {
  suffix=$1

  experiment_dir="${top_directory}/experiments"
  global_results_dir="${experiment_dir}/results${suffix}"

  data_exp="${global_results_dir}/data_exp.csv"
  step_by_step_aggregate="${global_results_dir}/step_by_step_mid${suffix}.csv"

  execution_time_rplot_script="${script_directory}/gen_exp_results.R"

  location_resulting_plot="${global_results_dir}/data_exp_plot.pdf"

  if [ ! -f ${time_data} ]
  then
    echo "Failed execution time plotting : no time data"
  else
    Rscript "${execution_time_rplot_script}" "${data_exp}" "${step_by_step_aggregate}"
    # pdftk Rplots.pdf cat 1 output "${global_results_dir}/selection_time.pdf"
    # pdftk Rplots.pdf cat 2 output "${global_results_dir}/sese_selection_vs_overall.pdf"
    # pdftk Rplots.pdf cat 3 output "${global_results_dir}/MID_estimation.pdf"
    # pdftk Rplots.pdf cat 4 output "${global_results_dir}/security_threshold.pdf"
    # mv "Rplots.pdf" "${location_resulting_plot}"
    mv "MAW_sese_per_bench.csv" "${global_results_dir}/MAW_sese_per_bench${suffix}.csv"
  fi
}

function gen_full_level_results {
  experiment_dir="${top_directory}/experiments"

  data_exp_full_level_aggregate="${experiment_dir}/data_exp_full.csv"
  step_by_step_full_level_aggregate="${experiment_dir}/step_by_step_mid_full.csv"

  R_script="${script_directory}/gen_full_results.R"

  if [ ! -f "${data_exp_full_level_aggregate}" -o ! -f ${step_by_step_full_level_aggregate} ]
  then
    echo "Impossible to generate the full results : missing an aggregate"
  else
    cd "${experiment_dir}"

    Rscript "${R_script}" "${data_exp_full_level_aggregate}" "${step_by_step_full_level_aggregate}"
    cd "${top_directory}"
  fi
}

function gen_results {
  echo "***** GENERATE RESULTS *****"

  echo -e "  *** FULL LEVEL ***"
  gen_full_level_results

  for suffix in "${!experiments_options[@]}"
  do
    echo -e "  *** EXP LEVEL : ${suffix} ***"
    gen_exp_level_results "${suffix}"

    for benchmark_name in "${benchmarks[@]}"
    do
      declare -n benchmark=${benchmark_name}

      for bench in "${!benchmark[@]}"
      do
        echo -e "  *** BENCH LEVEL : ${bench} ***"
        gen_bench_level_results "${suffix}" "${bench}"
      done
    done
  done
}

function aggregate_benchs_data {
  echo -e "***** AGGREGATION *****"

  experiment_dir="${top_directory}/experiments"

  data_exp_full_level_aggregate="${experiment_dir}/data_exp_full.csv"
  step_by_step_full_level_aggregate="${experiment_dir}/step_by_step_mid_full.csv"

  declare -n exp_opt="experiments_options"

  for suffix in "${!exp_opt[@]}"
  do
    echo -e "  *** ${suffix} ***"
    exp_results_dir="${experiment_dir}/results${suffix}"
    step_by_step_exp_level_aggregate="${exp_results_dir}/step_by_step_mid${suffix}.csv"
    data_exp_csv="${exp_results_dir}/data_exp.csv"

    echo -e "    BENCH LEVEL AGGREG"
    for benchmark_name in "${benchmarks[@]}"
    do
      declare -n benchmark=${benchmark_name}

      for bench in "${!benchmark[@]}"
      do
        echo -e "      ${bench}"

        bench_name=$(basename $bench ".exe")
    		program_dir=$(dirname $bench)

        program_results_dir="${exp_results_dir}/${bench_name}"
        bench_step_by_step_mid="${program_results_dir}/step_by_step_mid.csv"

        if [ ! -f ${bench_step_by_step_mid} ]
        then
          echo "Bench : ${bench} failed : no step by step MID data"
        else
          if [ ! -f ${step_by_step_exp_level_aggregate} ]
          then
            (head -n 1 ${bench_step_by_step_mid} | sed "s/\$/;program/") > ${step_by_step_exp_level_aggregate}
          fi
          (sed '1d' ${bench_step_by_step_mid} | sed "s/\$/;${bench_name}/") >> ${step_by_step_exp_level_aggregate}
        fi
      done
    done

    # Aggregate exp level step by step data into full level

    echo -e "    EXP LEVEL AGGREG"
    if [ ! -f ${step_by_step_exp_level_aggregate} ]
    then
      echo "experiment ${suffix} has failed : no step by step aggregate"
    else
      if [ ! -f ${step_by_step_full_level_aggregate} ]
      then
        (head -n 1 ${step_by_step_exp_level_aggregate} | sed "s/\$/;exp/") > ${step_by_step_full_level_aggregate}
      fi
      (sed '1d' ${step_by_step_exp_level_aggregate} | sed "s/\$/;${suffix}/") >> ${step_by_step_full_level_aggregate}
    fi

    # Aggregate exp level exp data into full level

    echo -e "    FULL LEVEL AGGREG"
    if [ ! -f ${data_exp_csv} ]
    then
      echo "${data_exp_csv}"
      echo "experiment ${suffix} has failed : no exp data"
    else
      if [ ! -f ${data_exp_full_level_aggregate} ]
      then
        (head -n 1 ${data_exp_csv} | sed "s/\$/;exp/") > ${data_exp_full_level_aggregate}
      fi
      (sed '1d' ${data_exp_csv} | sed "s/\$/;${suffix}/") >> ${data_exp_full_level_aggregate}
    fi

  done
}

function run_benchmarks {
  declare -n benchmark=$1
  declare -n exp_opt="experiments_options"
  declare -n exp_sub="experiments_max_subSeses"

  for bench in "${!benchmark[@]}"
  do
    experiment_dir="${top_directory}/experiments"

    bench_name=$(basename $bench ".exe")
    program_dir=$(dirname $bench)

    echo ""
    echo "***["  $(echo ${bench_name} | tr '[:lower:]' '[:upper:]') "]***"
    echo ""

    for suffix in "${!exp_opt[@]}"
    do
      opt=${exp_opt[${suffix}]}
      max_subSese=${exp_sub[${suffix}]}

  		startup_file="${program_dir}/startup/${bench_name}${suffix}.py"

      global_results_dir="${experiment_dir}/results${suffix}"
      program_results_dir="${global_results_dir}/${bench_name}"

      stdout_file="${program_results_dir}/stdout.txt"
      stderr_file="${program_results_dir}/stderr.txt"

      gen_startup_file "${bench}" "${benchmark[${bench}]}" "${suffix}" "${max_subSese}"
      python3 pymid/Controller.py ${opt} ${startup_file} 2> "${stderr_file}" 1> "${stdout_file}"

    done
  done
}

for benchmark_name in ${benchmarks[@]}
do
  run_benchmarks ${benchmark_name}
done

aggregate_benchs_data
gen_results
