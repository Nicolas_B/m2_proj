#!/bin/bash

set -e

declare -A gcc_option_per_benchmarks=(
["mdh"]="-DAIS_GEN_ON"
["polybench"]="-Ibenchmarks/src/polybench/utilities/ -DAIS_GEN_ON -O0 -DSMALL_DATASET -DPOLYBENCH_STACK_ARRAYS -DPOLYBENCH_NO_FLUSH_CACHE -DDATA_TYPE_IS_DOUBLE benchmarks/src/polybench/utilities/polybench.c"
)

function gen_ais_from_output {
  bench_path=$1
  benchmark_name=$2

  gcc_options=${gcc_option_per_benchmarks[${benchmark_name}]}

  bench_name=$(basename "${bench_path}" ".c")
  bench_dir=$(dirname "${bench_path}")
  benchmarks_dir=$(dirname "${bench_dir}")
  benchmarks_dir_name=$(basename "${benchmarks_dir}")

  include_dir="${bench_dir}"
  include_ais_gen="benchmarks/src/${benchmark_name}/includes"
  tmp_dir=".tmp_ais_from_output"

  script_output_to_ais_path="tools/bound_from_print.py"

  mkdir -p ${tmp_dir}

  gcc_output="${tmp_dir}/${bench_name}"
  loop_output="${tmp_dir}/${bench_name}_output.txt"
  ais_output="benchmarks/ais/${benchmark_name}/${bench_name}.ais"

  gcc -I${include_dir} -I${include_ais_gen} ${gcc_options} ${bench_path} -o "${gcc_output}"
  ./${gcc_output} > ${loop_output}
  python3 ${script_output_to_ais_path} ${loop_output} > ${ais_output}
  rm -r ${tmp_dir}
}

