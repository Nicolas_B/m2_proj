from pyparsing import *

s = Suppress

end = s(Keyword("$end"))

identifier = Word(printables)

content = SkipTo(end)
content.setParseAction(tokenMap(str.strip))

date = s(Keyword("$date")) + content("date") + end
version = s(Keyword("$version")) + content("version") + end

unit = oneOf('s ms ns us ps fs')
timescale = Group(s(Keyword("$timescale")) + Word(nums)("timescale_num") + unit("timescale_unit") + end)

scope_start = s(Keyword("$scope")) + s(Word(alphas)) + \
              content("scope_name") + end
scope_end = s(Keyword("$upscope")) + end

scope_content = Forward()
scope = Group(scope_start + scope_content + scope_end)

signal_name = Word(printables)('name') + Optional(s("[") + Word(nums)("idx") + s("]"))

signal_def = s(Word(alphas)) + s(Word(nums)) + \
             identifier("signal_id") + signal_name

signal = Group(s(Keyword("$var")) + signal_def + end)

scope_content << ZeroOrMore( signal("signals*") | scope("scopes*") )

definitions = Group(ZeroOrMore(scope)("scopes") + s(Keyword("$enddefinitions")) + end)

timestamp = s("#") + Word(nums)
commands = s("$") + Word(alphanums)
interest_value = Group(oneOf('0 1')("new_value") + identifier("id") + s(WordEnd()))
thrash_value = Group(oneOf('x u z w')("new_value") + identifier("id") + s(WordEnd()))


change = Group(timestamp("timestamp") + OneOrMore(interest_value("flips*") | s(thrash_value) | s(commands) ) + s(Optional(end)))
steps = ZeroOrMore(change)("steps") + s(StringEnd())

vcd = Optional(date) + Optional(version) + timescale + \
          definitions("definitions") + steps
