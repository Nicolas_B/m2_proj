# coding=utf-8
import sys
import grammar

clk_wire = "testbench.d3.cpu(0).leon3.vhdl.p0.iu.clk"
annul_wire = "testbench.d3.cpu(0).leon3.vhdl.p0.cpi.e.annul"
bpmiss_wire = "testbench.d3.cpu(0).leon3.vhdl.p0.iu.dbgo.bpmiss"
pc_wire = "testbench.d3.cpu(0).leon3.vhdl.p0.cpi.e.pc"


class Signal( object ) :
    def __init__( self, name, identifier, idx=-1 ) :
        self.name = name
        self.id = identifier
        self.idx = idx if idx != -1 else None
        self.values = [ ]

    def add_value( self, new_value, timestamp ) :
        last_timestamp = -1 if len( self.values ) == 0 else self.values[ -1 ][ 0 ]
        timestamp = int( timestamp )

        try :
            new_value = int( new_value )
        except ValueError :
            return

        if last_timestamp >= timestamp :
            raise Exception( "The timestamp %d is inferior to the previous registered timestamp %d" % (timestamp, last_timestamp) )

        self.values.append( (timestamp, new_value) )

    def retrieve_value( self, timestamp ) :
        # Could be replaced with a dichotomic search

        last_reach = 0
        for idx in range( len( self.values ) ) :
            if self.values[ idx ][ 0 ] > timestamp :
                if idx == 0 :
                    raise Exception( "Trying to retrieve a value inferior to the starting point of log" )
                else :
                    return self.values[ idx - 1 ][ 1 ]
            else :
                last_reach = idx

        return self.values[ last_reach ][ 1 ]


class WireGroup( object ) :
    def __init__( self, name ) :
        self.name = name
        self.signals = { }

    def get_value( self, timestamp ) :
        value: int = 0
        for i in self.signals.keys() :
            value += self.signals[ i ].retrieve_value( timestamp ) << i

        return value

    def is_set( self ) :
        for i in self.signals.keys() :
            if len( self.signals[ i ].values ) == 0 :
                return False

        return True

    def get_last_value( self ) :
        value: int = 0
        for i in self.signals.keys() :
            value += self.signals[ i ].values[ -1 ][ 1 ] << i

        return value


class VCDFile( object ) :

    def __init__( self, filename, select=None ) :
        self.filter = select if select is not None else [ ]

        parse_tree = grammar.vcd.parseFile( filename )
        self.parse_tree = parse_tree

        self.date = parse_tree.date
        self.version = parse_tree.version

        self.wire_groups = { }
        self.id_map = { }

        self.parse_definitions( parse_tree.definitions )

    def generate_pcs( self, stepfiles ) :
        global pc_wire, clk_wire, annul_wire, bpmiss_wire

        annul: int = 0

        pc: int = 0
        clk: int = 0

        rising_edge: bool = False
        bpmiss_set: bool = False

        added_pc: int = 0

        for step_file in stepfiles:

            sys.stderr.write("Parsing file %s\n" % step_file)
            parse_tree = grammar.steps.parseFile(step_file)
            steps = parse_tree.steps

            for step in steps :

                previous_clk = clk
                previous_annul = annul

                rising_edge: bool = False

                for flip in step.flips :
                    sig_id = flip.id
                    sig_new_value = int(flip.new_value)

                    if sig_id in self.id_map :
                        name, idx = self.id_map[ sig_id ]

                        if name == clk_wire:
                            clk = sig_new_value

                        elif name == annul_wire:
                            annul = sig_new_value

                        elif name == pc_wire:
                            pc -= pc & ( 1 << idx )
                            pc += ( sig_new_value << idx )

                        # Flag bit check
                        if name == clk_wire and sig_new_value == 1 and previous_clk == 0 :
                            rising_edge = True

                        if name == bpmiss_wire and sig_new_value == 1 :
                            bpmiss_set = True

                        if name == annul_wire and sig_new_value == 0 and previous_annul == 1 :
                            bpmiss_set = False
                            annul = sig_new_value

                # Add pcs to the list
                if rising_edge :
                    # The clock is on a rising edge
                    if not bpmiss_set or annul == 0:
                        added_pc += 1
                        print( "%s" % hex(pc) )

        sys.stderr.write("Total pc : %d\n" % added_pc)

    # def parse_steps( self, steps ) :
    #     for step in steps :
    #         timestamp = step.timestamp[ 0 ]
    #         for flip in step.flips :
    #             sig_id = flip.id
    #             sig_new_value = flip.new_value
    #
    #             if sig_id in self.id_map :
    #                 name, idx = self.id_map[ sig_id ]
    #                 self.wire_groups[ name ].signals[ idx ].add_value( sig_new_value, timestamp )
    #
    #         print( "@%d, pc %s" % (int( step.timestamp[ 0 ] ), hex(
    #             self.wire_groups[ "testbench.d3.cpu(0).leon3.vhdl.p0.iu.r.f.pc" ].get_last_value() )) )

    def parse_definitions( self, definitions ) :
        def parse_signal( current_scope, signal ) :
            signal_id = signal.signal_id
            name = ".".join( current_scope + [ signal.name ] )
            idx: int = int( signal.idx ) if signal.idx != "" else 0

            keep = False if len( self.filter ) != 0 else True

            for f in self.filter :
                if f in name :
                    keep = True
                    break

            if not keep :
                return

            self.id_map[ signal_id ] = (name, idx)

            S = Signal( name, signal_id, idx )
            if name in self.wire_groups :
                if idx == "" :
                    raise Exception( "Trying to add a signal to an existing wire group without idx " )

                self.wire_groups[ name ].signals[ idx ] = S
            else :
                self.wire_groups[ name ] = WireGroup( name )
                if idx == "" :
                    self.wire_groups[ name ].signals[ 0 ] = S
                else :
                    self.wire_groups[ name ].signals[ idx ] = S

        def parse_scope( current_scope, to_parse ) :
            current_scope.append( to_parse.scope_name )

            for inner_scope in to_parse.scopes :
                parse_scope( current_scope, inner_scope )

            for signal in to_parse.signals :
                parse_signal( current_scope, signal )

            current_scope.pop()

        for scope in definitions.scopes :
            parse_scope( [ ], scope )


if __name__ == "__main__" :
    if len( sys.argv ) < 2 :
        print( "A filename should be given" )
        exit( -1 )

    vcdFile = VCDFile( sys.argv[ 1 ], select=[ pc_wire, clk_wire, annul_wire, bpmiss_wire ] )
    vcdFile.generate_pcs( sys.argv[ 2: ] )
