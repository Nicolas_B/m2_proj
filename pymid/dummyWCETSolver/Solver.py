# -*- coding: utf-8 -*-

from collections import namedtuple
import time
import random

from pymid.analysis.predicats import is_secured

SeseAisMetadata = namedtuple( 'SeseAisMetadata', 'startAdd endAdd id stopDecodingAdd hitTheEnd' )


class dummyWCETSolver( object ) :

    def __init__( self, project ) :
        self.seseTime = dict()
        self.project = project
        self.timeMIT = 0
        self.MITCall = 0
        random.seed()

    def init_program( self, program_options, program ) :
        None

    def pre_hook( self ) :
        for P in self.project.programs :
            for function in filter( is_secured, P.icfg ) :
                for sese in filter( lambda x : not x.is_root(), function.get_pdst() ) :
                    if sese.is_domain() :
                        self.seseTime[sese] = 0
                    else :
                        self.seseTime[sese] = random.randrange( 100 )

    def _compute_one_sese( self, sese ) :
        self.MITCall += 1
        function = sese.function
        program = function.program

        descendant = program.ipdst.get_descendant( sese )

        sese.subSeseWCET = dict()
        sese.WCET_OOC = self.seseTime[sese]
        for s in descendant :
            if s == sese :
                continue
            if s.selected :
                continue
            if s.is_domain() and s.doNotProcess :
                continue
            sese.WCET_OOC += self.seseTime[s]
            sese.subSeseWCET[s] = self.seseTime[s]

    def compute_seses( self, prefix, seses=None ) :
        start_time = time.time()
        if seses is not None :
            for s in seses :
                self._compute_one_sese( s )

        else :
            for P in self.project.programs :
                for function in filter( is_secured, P.icfg ) :
                    for sese in filter( lambda x : not x.is_root(), function.get_pdst() ) :
                        if sese.is_domain() and sese.doNotProcess :
                            continue

                        self._compute_one_sese( sese )

        elapsed_time = time.time() - start_time
        self.timeMIT += elapsed_time

    def recover_sese_metadata( self, sese ) :
        """ Analyse the sese region and the cfg to decide where to place the end point and other sese feature for aiT """

        function = sese.function

        hitTheEnd = None
        stopDecodingAdd = None

        """ Differentiate between domain and region for the id and the entry and exit edges """
        id_sese = str( sese )
        entryEdge = sese.get_entry_edge()
        exitEdge = sese.get_exit_edge()

        if entryEdge is None or exitEdge is None :
            return

        if entryEdge[0] == 'start' :
            """ We are at the beginning of a function, we must let 1 instruction free of sese for aiT"""
            startBBNum = entryEdge[1]

            """ Note : We consider that the first BB of a program is never 2 or less instructions """
            startAdd = function.cfg.bbs.BBs[startBBNum].start + 0x4

        else :
            startBBNum = entryEdge[1]
            startAdd = function.cfg.bbs.BBs[startBBNum].start

        if exitEdge[1] == 'end' :
            """ We are at the end of a function, we must let the return instruction (+ delay slot) free for aiT """
            endBBNum = exitEdge[0]

            """ Note : We consider that the last BB contains at least 3 instructions (1 inst + return + delay slot) """
            endAdd = function.cfg.bbs.BBs[endBBNum].end - 0x8
            hitTheEnd = True

        else :
            endBBNum = exitEdge[1]
            endBB = function.cfg.bbs.BBs[endBBNum]
            endAdd = endBB.start

            """ /!\ the > is very important because the delay slot is taken into account in the BB even if it is only a branch + delay slot"""
            if endBB.end - endBB.start == 4 :
                # Only one instruction + delay slot in the BB, aiT does not appreciate so we put the end 1 BB later
                # We suppose the instruction will always be a bn or ba instruction and thus there is only 1 neighbors node
                nextBbNum = list( function.cfg.get_graph().neighbors( endBBNum ) )[0]
                nextBb = function.cfg.bbs.BBs[nextBbNum]
                stopDecodingAdd = nextBb.start
            else :
                stopDecodingAdd = endAdd + 0x4

        sese.metadata = SeseAisMetadata( startAdd=startAdd, endAdd=endAdd, id=id_sese, stopDecodingAdd=stopDecodingAdd,
                                         hitTheEnd=hitTheEnd )
