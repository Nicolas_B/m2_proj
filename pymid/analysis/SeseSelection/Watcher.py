# -*- coding: utf-8 -*-

import pathlib

class Watcher( object ) :
    def __init__( self, path: pathlib.Path, name: str ) :
        self.memory = 0
        self.goal = 0
        self.sese_ids = None
        self.max_arity = 0
        self.max_stack = 0
        self.filePath = path.joinpath( name + '.csv' ).absolute()

        if not self.filePath.exists() :
            with open( str( self.filePath ), 'w' ) as f :
                """ Put data into the file """
                f.write(
                    "%s;%s;%s;%s;%s\n" % ("memory", "sese_ids", "maximal_mid", "maximal_arity", "maximal_stack") )

    def write( self ) :
        """ Put the data into the csv and reset the watcher data and add 1 to the round counter """

        with open( str( self.filePath ), 'a' ) as f :
            """ Put data into the file """
            f.write( "%d;%s;%d;%d;%d\n" % (
                self.memory, str(self.sese_ids), self.goal, self.max_arity, self.max_stack) )

    def pre_update_hook( self, solver, maximal_sese, new_selected ):
        """ For future analysis """
        None

    def post_update_hook( self, solver, maximal_sese, new_selected ):
        self.goal : int = solver.goal
        self.sese_ids : str = str(new_selected)
        self.memory : int = len(solver.selected)
        self.max_stack = solver.selector.compute_stack_size( solver, None )
        self.max_arity = solver.selector.compute_arity( solver, None )

        self.write()
