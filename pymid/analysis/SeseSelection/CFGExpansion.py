# -*- coding: utf-8 -*-

def expanse_cfg( cfg ) :
    mese = list()
    seme = list()
    meme = list()

    G = cfg.get_graph()

    """ Categorise the nodes """
    for n in G.nodes :
        if n == "start" or n == "end" :
            continue

        ind = G.in_degree( n )
        oud = G.out_degree( n )

        if ind >= 2 and oud >= 2 :
            meme.append( n )
        elif ind >= 2 :
            mese.append( n )
        elif oud >= 2 :
            seme.append( n )

    """ Break the nodes that can be broken """
    for n in mese :
        BB = cfg.bbs.BBs[n]
        if len( BB ) < 4 :
            continue
        split_add = BB.start + 0x8
        cfg.split_bb( n, split_add, split_add_to_new_bb=True )

    for n in seme :
        BB = cfg.bbs.BBs[n]
        if len( BB ) < 4 :
            continue
        split_add = BB.end - 0x4
        cfg.split_bb( n, split_add, split_add_to_new_bb=True )

    for n in meme :
        BB = cfg.bbs.BBs[n]
        if len( BB ) < 6 :
            continue
        split_add_start = BB.start + 0x8
        split_add_end = BB.end - 0x4
        n_split = cfg.split_bb( n, split_add_start, split_add_to_new_bb=True )
        cfg.split_bb( n_split, split_add_end, split_add_to_new_bb=True )
