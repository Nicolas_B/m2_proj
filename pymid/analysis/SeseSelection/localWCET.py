# -*- coding: utf-8 -*-

from functools import reduce
from collections import namedtuple
from typing import List, Union

from pymid.analysis.SeseSelection.Core import MemoryModel, SeseSelector
from pymid.utils import loggingConf
from pymid.analysis.PDST import SESEDomain, SESERegion


class SimpleMemoryModel( MemoryModel ) :
    def __init__( self, project ) :
        super().__init__( project )

    def memory_cost( self, selected: Union[List[Union[SESEDomain, SESERegion]], SESEDomain, SESERegion] ) -> int :
        """ Check for a __len__ function, if does not exist then check for a iterable and if not consider a unit """
        try :
            return len( selected )
        except :
            None

        try :
            return reduce( lambda x, y : x + 1, selected, 0 )
        except :
            None

        return 1


class localWcetSeseSelector( SeseSelector ) :
    logger = loggingConf.getLogger( "localWcetSeseSelector" )

    def __init__( self, project, memory: MemoryModel ) :
        super().__init__( project, memory )

    def _compute_potential_goal( self, sese, sub_sese, solver ) :
        gain = max( sub_sese.WCET_OOC, sese.WCET_OOC - sese.subSeseWCET[sub_sese] )
        if not self.memory.check_remaining_memory( self.memory.memory_cost( sub_sese ) ) :
            """ If there is no remaining memory """
            return -1

        if sub_sese.selected :
            return -1

        if not sub_sese.is_processable() :
            return -1

        if not self.check_stack_constraints( solver, sub_sese ):
            return -1

        if not self.check_arity_constraints( solver, sub_sese ):
            return -1

        return gain

    def select( self, solver, sese ) :
        potentials_map = { s : self._compute_potential_goal(
                                sese,
                                s,
                                solver )
                           for s in sese.subSeseWCET.keys() }

        potentials = filter( lambda s : potentials_map[s] > 0,
                             potentials_map
                             )

        self.logger.debug(
            "%s" % str( potentials_map ) )

        selected_sese = min( potentials,
                             key=lambda potent : potentials_map[potent],
                             default=None )

        if selected_sese is None :
            return list()

        return [ selected_sese ]
