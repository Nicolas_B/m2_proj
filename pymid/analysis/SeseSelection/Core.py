# -*- coding: utf-8 -*-

"""
This file contains the core classes to perform the SESE selection :
* An interface for the different possible analyses
* A generic sese selector class that can integrate an analysis
* A generic sese selection solver that use the previous class to return a result
"""

from functools import reduce
import time
from typing import List, Optional, Union
import networkx as nx

from pymid.analysis.PDST import SesePart, SESEDomain, SESERegion
from pymid.analysis.predicats import secured_function_iter
from pymid.analysis.SeseSelection.Watcher import Watcher
from pymid.utils import loggingConf
from pymid.utils.graphUtility import dfs_conditional_stop

class SelectedGraph( nx.DiGraph ):

    def __init__(self, solver, *args, **kargs):
        super().__init__(*args, **kargs)
        self.solver = solver

    def build( self ):
        ipdst = self.solver.project.programs[ 0 ].ipdst.get_graph()

        for root in self.solver.selected :
            def arity_predicat( s ) :
                nonlocal root
                return not s == root and s.selected

            for sese in dfs_conditional_stop( ipdst, root, arity_predicat, arity_predicat ):
                self.add_edge(root, sese)

    def get_extra_memory( self ) -> int:
        if len( self.nodes ) == 0:
            self.build()

        extra_memory: int = 0
        for sese in self.nodes :
            if self.in_degree( sese ) > 1:
                extra_memory += self.in_degree( sese ) - 1

        return extra_memory

class MemoryModel( object ) :
    """
    Model the memory relation between sese regions and the cost of selecting them
    """

    def __init__( self, project ) :
        self.project = project
        self.remainingMemory = self.project.memory_space
        self.totalMemory = self.project.memory_space
        self.maximalMemoryRequired = None

    def memory_cost( self, selected ) -> int :
        """
        Compute the cost (in terms of memory) of selecting these sese regions
        """
        return -1

    def get_remaining_memory( self ) -> int :
        """
        Compute the remaining memory
        """
        return self.remainingMemory

    def update( self, maximal_sese: Optional[ Union[ SESEDomain, SESERegion ] ],
                selected: List[ Union[ SESEDomain, SESERegion ] ] ) :
        if self.project.memory_space is not None :
            cost = self.memory_cost( selected )

            if not self.check_remaining_memory( cost ) :
                raise Exception( "Not enough memory to select these SESE : %s" % selected )

            self.remainingMemory -= cost

    def suppress_domain( self, domain ) :
        """ Allow to suppress a domain when all its regions but one are already selected"""
        if self.project.memory_space is not None :
            cost = self.memory_cost( [ domain ] )
            self.remainingMemory += cost

    def check_remaining_memory( self, cost ) :
        if self.project.memory_space is None :
            return True
        return self.remainingMemory >= cost


class SeseSelector( object ) :
    """
    Implement a strategy to select Sese regions in order to solve the Sese selection problem
    """
    logger = loggingConf.getLogger( "Selector" )

    def __init__( self, project, memory_model: MemoryModel ) :
        self.project = project
        self.memory = memory_model
        self.watcher = None

    def pre_hook( self, solver ) :
        """
        Compute all the data the selector needs before being able to start selecting regions
        """
        pass

    def update( self, maximal_sese: Optional[ SesePart ], selected: List[ SesePart ] ) :
        """
        Update to the selection
        """
        pass

    def compute_stack_size( self, solver, sese : Union[None, SESERegion, SESEDomain] ) -> int:
        g_stack = solver.project.programs[0].ipdst.get_graph().copy()

        for s in g_stack.nodes() :
            if s != sese and s.selected :
                for v, _ in g_stack.in_edges( s ) :
                    g_stack[ v ][ s ][ "selected" ] = 1

        if sese is not None and not sese.selected :
            for v, _ in g_stack.in_edges( sese ) :
                g_stack[ v ][ sese ][ "selected" ] = 1

        max_stack = nx.algorithms.dag.dag_longest_path_length( g_stack, "selected", 0 )
        return max_stack + 1

    def check_stack_constraints( self, solver, sese ) -> bool :
        if self.project.stack_space is None :
            return True

        max_stack = self.compute_stack_size( solver, sese )
        return max_stack <= self.project.stack_space

    def compute_arity( self, solver, sese ) -> int:
        ipdst = solver.project.programs[0].ipdst.get_graph()
        max_arity = 0

        for sel in solver.selected :
            def arity_predicat( s ) :
                nonlocal sese, sel
                if s == sel :
                    return False
                if s != sese and s.selected :
                    return True
                if s == sese :
                    return not s.selected

            arity = len( list( dfs_conditional_stop( ipdst, sel, arity_predicat, arity_predicat ) ) )
            max_arity = max(max_arity, arity)

        return max_arity

    def check_arity_constraints( self, solver, sese ) -> bool :
        if self.project.max_subSese is None :
            return True

        ipdst = sese.function.program.ipdst.get_graph()

        for sel in solver.selected :
            def arity_predicat( s ) :
                nonlocal sese, sel
                if s == sel :
                    return False
                if s != sese and s.selected :
                    return True
                if s == sese :
                    return not s.selected

            arity = len( list( dfs_conditional_stop( ipdst, sel, arity_predicat, arity_predicat ) ) )

            if arity > self.project.max_subSese :
                return False

        return True

    def select( self, *args, **kargs ) -> List[ Union[ SESEDomain, SESERegion ] ] :
        """
        Generate a list of sese regions or domain that will be selected
        """
        return [ ]

    def suppress_domain( self, domain: SESEDomain ) :
        """ Allow to suppress a domain when all its regions but one are already selected"""
        pass


class SeseSelectionSolver( object ) :
    """
    Solve the problem of Sese Selection over a whole project
    """

    logger = loggingConf.getLogger( "Solver" )

    def __init__( self, project, memory_model: MemoryModel, selector: SeseSelector, watcher: Watcher = None ) :
        self.selector = selector
        self.project = project
        self.memory = memory_model
        self.selected = list()
        self.goal = None
        self.updateCounter = 0
        self.genericAisCounter = 0
        self.watcher = watcher
        selector.watcher = watcher
        self.timeSolving = 0
        self.maximal_sese = None

    def pre_hook( self ) :
        """
        Compute pre-analysis required to perform the analysis
        """

        self.logger.info( """ Running the preHooks for the analysis """ )

        self.project.WCETSolverInterface.pre_hook()

        """ Provide metadata for each sese """
        for P in self.project.programs :
            for function in secured_function_iter( P ) :
                for sese in filter( lambda x : x.is_processable(), function.get_pdst() ) :
                    self.project.WCETSolverInterface.recover_sese_metadata( sese )
                    sese.selected = False

        """ Compute the WCET_OOC of each sese """
        self.project.WCETSolverInterface.compute_seses( prefix="preHook_" )

        """ Now applies the pre_hook of the selector """
        self.selector.pre_hook( self )

    def get_maximal_selected( self ) -> Union[ SESEDomain, SESERegion ] :
        """ Return the selected region which represent the bottleneck of the selection """
        return max( self.selected, key=lambda selected : selected.WCET_OOC )

    def update_domain( self, maximal_sese, new_selected: List[ Union[ SESEDomain, SESERegion ] ] ) :
        for sese in new_selected :
            pdst = sese.function.pdst

            for domain in filter( lambda s : s.usable_domain, pdst.get_graph().predecessors( sese ) ) :
                if sese not in domain.seses :
                    break

                """ The newly selected sese is part of a domain """
                selected_sese_index: int = domain.seses.index( sese )
                front_guard: bool = False  # Is sese a front guard to a non computable region
                rear_guard: bool = False  # Is sese a rear guard to a non computable region

                if selected_sese_index - 1 >= 0 :
                    rear_sese = domain.seses[ selected_sese_index - 1 ]
                    if not rear_sese.domain_computable :
                        rear_guard = True

                if selected_sese_index + 1 < len( domain.seses ) :
                    front_sese = domain.seses[ selected_sese_index + 1 ]
                    if not front_sese.domain_computable :
                        front_guard = True

                if front_guard :
                    # Verify if there a previous front guard which protects anyway
                    for i in range( selected_sese_index ) :
                        if not domain.seses[ i ].domain_computable :
                            front_guard = False

                if rear_guard :
                    # Verify if there a next rear guard which protects anyway
                    for i in range( len( domain.seses ) - 1, selected_sese_index, -1 ) :
                        if not domain.seses[ i ].domain_computable :
                            rear_guard = False

                """
                If the sese is nor a front nor a rear guard, then the sese can be removed
                from the domain
                """

                if not front_guard and not rear_guard :
                    """ The sese is removable from the domain """
                    domain.remove_sese( sese )

                    # if domain.is_removable() and self.selector.check_arity_constraints( self, domain ) and self.selector.check_stack_constraints( self, domain ):
                    #     """
                    #     Eliminate the domain and replace it with the region inside (cannot happen if there is a
                    #     non computable region inside the sese
                    #     """
                    #     domain_only_sese = domain.seses_remaining[ 0 ]
                    #
                    #     if not domain_only_sese.computable :
                    #         raise Exception( """ Domain elimination of a domain containing non computable sese """ )
                    #
                    #     domain.doNotProcess = True
                    #
                    #     if domain.selected :
                    #         self.selected.remove( domain )
                    #         self.memory.suppress_domain( domain )
                    #         self.selector.suppress_domain( domain )
                    #
                    #         """ We replace the domain by the remaining region """
                    #         self.selector.update( maximal_sese, [ domain_only_sese ] )
                    #         self.memory.update( maximal_sese, [ domain_only_sese ] )
                    #
                    #         self.selected.append( domain_only_sese )
                    #         domain_only_sese.selected = True
                    #         domain.selected = False

    def update( self, maximal_sese: Optional[ Union[ SESEDomain, SESERegion ] ],
                new_selected: List[ Union[ SESEDomain, SESERegion ] ] ) :
        """
        Update the goal with the new information provided after the selection of some regions
        and recompute all the data
        """
        self.logger.info( """ Updating the information of the seses """ )

        if self.watcher is not None :
            self.watcher.pre_update_hook( self, maximal_sese, new_selected )

        for sese in new_selected :
            if not sese.is_processable() :
                raise Exception( """ Tried to select a non processable sese : %s """ % str( sese ) )

        self.updateCounter += 1

        self.selector.update( maximal_sese, new_selected )
        self.memory.update( maximal_sese, new_selected )

        for sese in new_selected :
            self.selected.append( sese )
            sese.selected = True

        self.update_domain( maximal_sese, new_selected )

        if maximal_sese is not None :
            program = maximal_sese.function.program
        else :
            program = new_selected[ 0 ].function.program

        seseToUpdate = list(
            filter( lambda s : s.is_processable(),
                    reduce( set.union, [ program.ipdst.get_ancestors( s ) for s in new_selected ] ) ) )

        for sese in seseToUpdate :
            if not sese.is_processable() :
                raise Exception( """ Tried to update a sese that should not be processed : %s""" % str( sese ) )

        self.project.WCETSolverInterface.compute_seses( prefix="update_%d_" % self.updateCounter, seses=seseToUpdate )

        """ Update the goal of the analysis """
        max( self.selected, key=lambda selected : selected.WCET_OOC )
        self.maximal_sese: Union[ SESEDomain, SESERegion ] = self.get_maximal_selected()

        if self.maximal_sese is None :
            raise Exception( """ Unable to find a maximal selected sese """ )

        if self.goal is None:
            self.goal = self.maximal_sese.WCET_OOC
        else:
            if self.goal > self.maximal_sese.WCET_OOC:
                self.goal = self.maximal_sese.WCET_OOC

        if self.watcher is not None :
            self.watcher.post_update_hook( self, maximal_sese, new_selected )

    def solve( self ) :
        """
        Call the associated seseSelector to select seseRegion until there is no memory left
        Each time new sese regions are selected, aiT is called to adapt the cost of the node and recompute the current maximal
        """
        self.timeSolving = time.time()

        self.pre_hook()

        """ Else select the first regions of each program and update """
        first_selected = list()

        for P in self.project.programs :
            main = P.get_function_from_name( P.starting_point )
            pdst = main.get_pdst()
            for sese in pdst.get_graph().successors( pdst.get_root() ) :
                first_selected.append( sese )

        self.update( None, first_selected )

        while self.memory.check_remaining_memory( 1 ) :
            """ Recover the sese region that is the current bottleneck region """
            new_selected_seses: List[ Union[ SESEDomain, SESERegion ] ] = self.selector.select( self,
                                                                                                self.maximal_sese )

            if len( new_selected_seses ) == 0 :
                """ Did not find a sese that respects the constraints """
                break

            self.update( self.maximal_sese, new_selected_seses )

        self.timeSolving = time.time() - self.timeSolving
