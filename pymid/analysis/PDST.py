# -*- coding: utf-8 -*-
from typing import Dict, Set, Tuple, Optional, List, Union

import networkx as nx
import time

from pymid.utils import loggingConf
from pymid.utils.graphUtility import GraphClass, dfs_conditional_stop
import pymid.utils.loggingConf
from pymid.analysis.predicats import is_secured
from pymid.analysis.program import Function, Program


class SesePart( object ) :
    """
    Base class for constructing Sese regions and Sese domains
    """

    def __init__( self, function: Function ) :
        self.doNotProcess: bool = False
        self._computable: bool = False
        self.metadata = None
        self.subSeseWCET: Dict[ SesePart, int ] = dict()
        self._function: Function = function
        self.loop: Optional[ int ] = None
        self.selected = False

    def is_root( self ) -> bool :
        return False

    def is_domain( self ) -> bool :
        return False

    def is_region( self ) -> bool :
        return False

    def get_entry_edge( self ) :
        return None

    @property
    def computable( self ) -> bool :
        return self.is_region() and self._computable

    @property
    def loopable( self ) -> bool :
        return self.loop is not None

    @property
    def domain_computable( self ) -> bool :
        """ Does the domain remains computable if the sese is at the beginning or the end """
        return self.computable and self.loop is None

    @computable.setter
    def computable( self, prevent: bool ) :
        if self.computable :
            self._computable = prevent

    @property
    def usable_domain( self ) -> bool :
        return self.is_domain() and not self.doNotProcess

    @usable_domain.setter
    def usable_domain( self, new_value ) :
        if self.usable_domain :
            self.doNotProcess = not new_value

    def is_processable( self ) -> bool :
        if self.is_root() :
            return False
        if self.is_region() and not self.computable :
            return False
        if self.is_domain() and self.doNotProcess :
            return False

        return True

    def get_exit_edge( self ) :
        return None

    @property
    def function( self ) -> Function :
        return self._function


class SESERoot( SesePart ) :
    """
    Root node of the PDST
    """

    def __init__( self, function ) -> None :
        super().__init__( function )
        self.nodes = set()

    def is_root( self ) -> bool :
        return True

    def __str__( self ) -> str :
        return "SESERoot_%s" % str(self.function)

    def __repr__( self ) -> str :
        return self.__str__()


class SESERegion( SesePart ) :
    """
    A canonical SESE region
    """

    last_num = None

    def __init__( self, num: Tuple[ int, int ], entry, function: Function ) :
        super().__init__( function )

        self.num: Tuple[ int, int ] = num
        self.entry: Tuple[ int, int ] = entry
        self.exit = None
        self.nodes: Set[ int ] = set()
        self._computable = True
        self.to_adapt = True

        if SESERegion.last_num is None :
            SESERegion.last_num = num
        else :
            SESERegion.last_num = (max( SESERegion.last_num[ 0 ], num[ 0 ] ), max( SESERegion.last_num[ 1 ], num[ 1 ] ))

    @classmethod
    def get_new_num( cls ) -> Tuple[int, int]:
        if cls.last_num is None:
            raise Exception("No last_num")
        else:
            num = (cls.last_num[0] + 1, cls.last_num[1] + 1)
            cls.last_num = num
            return num

    def is_region( self ) -> bool :
        return True

    def get_entry_edge( self ) :
        return self.entry

    def get_exit_edge( self ) :
        return self.exit

    def __str__( self ) -> str :
        program = self.function.program
        return "Region_%s_%d_%d" % (program.name, self.num[ 0 ], self.num[ 1 ])

    def __repr__( self ) -> str :
        return self.__str__()


class SESEDomain( SesePart ) :
    """
    A canonical SESE domain (i.e. the maximal sized SESE domains)
    """

    cls_seed: int = 0

    def __init__( self, cls: int, function ) :
        super().__init__( function )

        self.seses = list()
        self.seses_remaining = list()
        self.number_uncomputable = 0
        self.cls = cls
        self.doNotProcess = False

        SESEDomain.cls_seed = max( cls, SESEDomain.cls_seed )

    @staticmethod
    def get_cls_seed() -> int :
        SESEDomain.cls_seed += 1
        return SESEDomain.cls_seed

    def is_domain( self ) -> bool :
        return True

    def get_entry_edge( self ) :
        return self.seses[ 0 ].entry

    def get_exit_edge( self ) :
        return self.seses[ -1 ].exit

    def is_removable( self ) -> bool :
        return len( self.seses_remaining ) <= 1 and self.number_uncomputable == 0

    def add_sese( self, sese: SESERegion ) :
        self.seses.append( sese )
        self.seses_remaining.append( sese )

    def remove_sese( self, sese: SESERegion ) :
        if sese not in self.seses :
            raise Exception(
                """Trying to remove a sese %s that does not belongs to the domain %s """ % (str( sese ), str( self )) )

        if not sese.computable :
            raise Exception(
                """Trying to remove an uncomputable sese %s from domain %s""" % (str( sese ), str( self )) )

        self.seses_remaining.remove( sese )

    def hard_remove_sese( self, sese: SESERegion ) :
        self.remove_sese( sese )
        self.seses.remove( sese )

    def __str__( self ) -> str :
        program = self.function.program
        return "Domain_%s_%d" % (program.name, self.cls)

    def __repr__( self ) -> str :
        return self.__str__()


class PDST( GraphClass ) :
    """
    The Program Domain Structure Tree of a function (containing SESE regions and SESE Domain)
    """

    def __init__( self, function: Function ) :
        super().__init__( "DiGraph" )

        self._function = function

        self.root = SESERoot( function )
        self.graph.add_node( self.root )

    def get_root( self ) -> SESERoot :
        return self.root

    def get_ancestors( self, sese: SesePart ) -> List[ Union[ SESEDomain, SESERoot, SESERegion ] ] :
        """ Return the ancestors SESE contained in the PDST """
        graph = self.get_graph()

        if sese not in graph :
            raise Exception( "%s not in %s" % (str( sese ), str( self._function )) )

        return nx.algorithms.dag.ancestors( graph, sese )


class IPDST( GraphClass ) :
    """
    The graph inter-procedural of domain structure tree
    """

    logger = loggingConf.getLogger( "IPDST" )

    def __init__( self, program: Program ) :
        super().__init__( "DiGraph" )
        self._program: Program = program
        self.root = None

        self.build()

    def build( self ) -> None :
        root_function = self._program.get_function_from_name( self._program.starting_point )
        self.root: SESERoot = root_function.pdst.get_root()
        G = self.get_graph()
        G.update( root_function.pdst.get_graph() )
        icfg = self._program.icfg

        already_copied: Set[ Function ] = set()
        already_copied.add( root_function )

        for u, v, n in icfg.get_graph().edges :
            if not is_secured( u ) or not is_secured( v ) :
                continue

            if v not in already_copied :
                G.update( v.pdst.get_graph() )
                already_copied.add( v )

            callingBB = icfg.get_graph()[ u ][ v ][ n ][ 'callBB' ]
            callingBBNum = u.cfg.bbs.BBs.index( callingBB )
            u_pdst = u.pdst
            v_root = v.pdst.root

            for sese in filter( lambda s : s.is_region() and callingBBNum in s.nodes, u_pdst ) :
                G.add_edge( sese, v_root )

    def get_descendant( self, sese: SesePart ) :
        descendants = set( filter( lambda x : not x.is_root(), nx.descendants( self.get_graph(), source=sese ) ) )
        descendants.add( sese )

        return descendants

    def get_ancestors( self, sese: SesePart ) :
        ancestors = set( nx.ancestors( self.get_graph(), source=sese ) )
        ancestors.add( sese )

        return ancestors

    @property
    def program( self ) :
        return self._program
