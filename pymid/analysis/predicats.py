# -*- coding: utf-8 -*-

from pymid.analysis.program import Program, Function

def is_secured( f : Function ) :
    return f.is_safe() and not f.is_recursive()

def secured_function_iter( program : Program ) :
    for f in filter( is_secured, program.icfg ) :
        yield f
