# -*- coding: utf-8 -*-

from collections import defaultdict
from typing import Any, Dict

from pymid.analysis.PDST import PDST, SESEDomain, SESERegion
from pymid.analysis.program import *
import pymid.utils.loggingConf

class _CEClass :
    number = 0

    def __init__( self ) :
        self.first = True
        self.backedge = None
        self.count = 0
        self.number = _CEClass.number
        _CEClass.number += 1

    def is_last( self ) :
        return self.count == 1

    def inc( self, bracket=None ) :
        if bracket is None :
            self.count += 1

        else :
            if self.backedge is not None and self.backedge != bracket :
                raise Exception( "ASSERT backedge" )
            if self.backedge is None :
                self.count += 1
            self.backedge = bracket

    def dec( self ) :
        self.count -= 1
        self.first = False

    def is_first( self ) :
        return self.first

    def __repr__( self ) :
        return self.__str__()

    def __str__( self ) :
        return "< _CEClass " + str( self.number ) + " >"


class PDSTBuilder( object ) :
    logger = pymid.utils.loggingConf.getLogger( "PDSTBuilder" )

    def __init__( self ) :
        """ Virtually private constructor """
        raise Exception( "The class PDSTBuilder is a static only class" )

    @staticmethod
    def build_pdst( f: Function ) -> PDST :
        PDSTBuilder.logger.info( """Constructing PDST of %s""" % f.get_name() )

        cfg_graph = f.cfg.get_graph()

        pdst = PDST( f )

        state : Dict[str, Any] = dict()
        state["dfsnum"] = dict()
        state["backedge"] = defaultdict( lambda : True )
        state["hi"] = defaultdict( lambda : -1 )
        state["bset"] = defaultdict( list )
        state["capping"] = defaultdict( lambda : False )
        state["recentSize"] = defaultdict( lambda : 0 )
        state["recentClass"] = dict()
        state["Class"] = defaultdict( lambda : None )
        state["nodes"] = [None] * len( cfg_graph )
        state["cur_dfsnum"] = 0

        cfg_graph.add_edge( 'end', 'start' )

        PDSTBuilder.logger.debug( """Computing DFS """ )
        PDSTBuilder._dfs( 'start', cfg_graph, state )

        PDSTBuilder.logger.debug( """DFS result : %s""" % str( state["nodes"] ) )
        PDSTBuilder.logger.debug( """Assigning classes to the edges """ )

        PDSTBuilder._assign_classes( cfg_graph, state )
        cfg_graph.remove_edge( 'end', 'start' )
        context = pdst.get_root()

        PDSTBuilder.logger.debug( """Building Program Structure Tree """ )
        PDSTBuilder._build_tree( 'start', context, cfg_graph, state, pdst, f )

        return pdst

    @staticmethod
    def _dfs( n, cfg_graph, state ) :
        cur_dfsnum = state["cur_dfsnum"]

        state["dfsnum"][n] = cur_dfsnum

        PDSTBuilder.logger.debug( """BB %s has DFSNUM %s""" % (str( n ), str( cur_dfsnum )) )

        state["nodes"][cur_dfsnum] = n
        state["cur_dfsnum"] += 1

        for t in cfg_graph.successors( n ) :
            if state["dfsnum"].get( t ) is None :
                state["backedge"][(n, t)] = False
                PDSTBuilder._dfs( t, cfg_graph, state )

        for s in cfg_graph.predecessors( n ) :
            if state["dfsnum"].get( s ) is None :
                state["backedge"][(s, n)] = False
                PDSTBuilder._dfs( s, cfg_graph, state )

    @staticmethod
    def _assign_classes( cfg_graph, state ) :
        for i in range( len( cfg_graph.nodes ) - 1, -1, -1 ) :
            hi0 = len( cfg_graph.nodes )
            hi1 = hi0
            hi2 = hi0

            n = state["nodes"][i]

            PDSTBuilder.logger.debug( """Processing BB: %s (dfsnum == %d)""" % (n, state["dfsnum"][n]) )

            # hi0 = min(t.dfsnum | (n,t) is a backedge)
            for t in cfg_graph.successors( n ) :
                dfsnum_t = state["dfsnum"][t]
                backedge = state["backedge"][(n, t)]
                PDSTBuilder.logger.debug( """Examining %s (dfsnum == %d)""" % (t, dfsnum_t) )
                if backedge and dfsnum_t < i and dfsnum_t < hi0 :
                    hi0 = dfsnum_t

            PDSTBuilder.logger.debug( """hi0 inter = %d""" % hi0 )

            for t in cfg_graph.predecessors( n ) :
                dfsnum_t = state["dfsnum"][t]
                backedge = state["backedge"][(t, n)]
                PDSTBuilder.logger.debug( """Examining %s (dfsnum == %d)""" % (t, dfsnum_t) )
                if backedge and dfsnum_t < i and dfsnum_t < hi0 :
                    hi0 = dfsnum_t

            PDSTBuilder.logger.debug( """hi0 = %d""" % hi0 )

            # hi1 = min( c.hi | c is a child of n )
            hichild = None

            for t in cfg_graph.successors( n ) :
                dfsnum_t = state["dfsnum"][t]
                hi = state["hi"][t]
                backedge = state["backedge"][(n, t)]
                PDSTBuilder.logger.debug( """Examining %s (dfsnum == %d)""" % (t, dfsnum_t) )
                if not backedge and hi < i and hi < hi1 and dfsnum_t > i :
                    hi1 = hi
                    hichild = t

            PDSTBuilder.logger.debug( """hi1 inter = %d""" % hi1 )

            for t in cfg_graph.predecessors( n ) :
                dfsnum_t = state["dfsnum"][t]
                hi = state["hi"][t]
                backedge = state["backedge"][(t, n)]
                PDSTBuilder.logger.debug( """Examining %s (dfsnum == %d)""" % (t, dfsnum_t) )
                if not backedge and hi < i and hi < hi1 and dfsnum_t > i :
                    hi1 = hi
                    hichild = t

            PDSTBuilder.logger.debug( """hi1 = %d""" % hi1 )
            PDSTBuilder.logger.debug( """hichild = %s""" % str( hichild ) )

            state["hi"][n] = min( hi1, hi0 )
            PDSTBuilder.logger.debug( """hi = %d""" % state["hi"][n] )

            if hichild :
                for t in cfg_graph.successors( n ) :
                    dfsnum_t = state["dfsnum"][t]
                    hi = state["hi"][t]
                    backedge = state["backedge"][(n, t)]
                    if hi < i and not backedge and hi < hi2 and dfsnum_t > i and t != hichild :
                        hi2 = hi

                PDSTBuilder.logger.debug( """hi2 inter = %d""" % hi2 )

                for t in cfg_graph.predecessors( n ) :
                    dfsnum_t = state["dfsnum"][t]
                    hi = state["hi"][t]
                    backedge = state["backedge"][(t, n)]
                    if hi < i and not backedge and hi < hi2 and dfsnum_t > i and t != hichild :
                        hi2 = hi

            PDSTBuilder.logger.debug( """hi2 = %d""" % hi2 )

            for t in cfg_graph.successors( n ) :
                dfsnum_t = state["dfsnum"][t]
                backedge = state["backedge"][(n, t)]
                if not backedge and dfsnum_t > i :
                    PDSTBuilder.logger.debug( """Merging bracket list for child: %s""" % str( t ) )
                    for b in state["bset"][t] :
                        state["bset"][n].append( b )

            for t in cfg_graph.predecessors( n ) :
                dfsnum_t = state["dfsnum"][t]
                backedge = state["backedge"][(t, n)]
                if not backedge and dfsnum_t > i :
                    PDSTBuilder.logger.debug( """Merging bracket list for child: %s""" % str( t ) )
                    for b in state["bset"][t] :
                        state["bset"][n].append( b )

            todel = []
            for b in state["bset"][n] :
                PDSTBuilder.logger.debug( """Considering edge : %s""" % str( b ) )
                s, t = b
                if (s == n and state["dfsnum"][t] > i) or (t == n and state["dfsnum"][s] > i) :
                    PDSTBuilder.logger.debug( """\tDeleting""" )
                    todel.append( b )

            for b in todel :
                s, t = b
                state["bset"][n].remove( b )
                if state["capping"][(s, t)] :
                    cfg_graph.remove_edge( s, t )
                    state["capping"][(s, t)] = False

            for t in cfg_graph.successors( n ) :
                dfsnum_t = state["dfsnum"][t]
                backedge = state["backedge"][(n, t)]
                if backedge and dfsnum_t < i :
                    state["bset"][n].append( (n, t) )

            for t in cfg_graph.predecessors( n ) :
                dfsnum_t = state["dfsnum"][t]
                backedge = state["backedge"][(t, n)]
                if backedge and dfsnum_t < i :
                    state["bset"][n].append( (t, n) )

            if hi2 < hi0 :
                PDSTBuilder.logger.debug( """Adding capping edge from %s to %s""" % (n, state["nodes"][hi2]) )
                cfg_graph.add_edge( n, state["nodes"][hi2] )
                state["capping"][(n, state["nodes"][hi2])] = True
                state["bset"][n].append( (n, state["nodes"][hi2]) )

            if i != 0 :
                edge = None
                for t in cfg_graph.successors( n ) :
                    dfsnum_t = state["dfsnum"][t]
                    backedge = state["backedge"][(n, t)]
                    if not backedge and dfsnum_t < i :
                        edge = (n, t)

                for t in cfg_graph.predecessors( n ) :
                    dfsnum_t = state["dfsnum"][t]
                    backedge = state["backedge"][(t, n)]
                    if not backedge and dfsnum_t < i :
                        edge = (t, n)

                if edge is None :
                    raise Exception( "ASSERT edge" )

                top_bracket = state["bset"][n][-1]
                current_size = len( state["bset"][n] )

                if state["recentSize"][top_bracket] != current_size :
                    state["recentSize"][top_bracket] = current_size
                    state["recentClass"][top_bracket] = _CEClass()

                state["Class"][edge] = state["recentClass"][top_bracket]
                PDSTBuilder.logger.debug( """Put edge %s in class %s""" % (str( edge ), str( state["Class"][edge] )) )
                state["Class"][edge].inc()

                if state["recentSize"][top_bracket] != len( state["bset"][n] ) :
                    raise Exception( "ASSERT recentSize != len(bset)" )

                if state["recentSize"][top_bracket] == 1 :
                    state["Class"][top_bracket] = state["Class"][edge]
                    PDSTBuilder.logger.debug(
                        """Put edge (top_bracket) %s in class %s""" % (str( edge ), str( state["Class"][edge] )) )
                    if top_bracket != ('end', 'start') :
                        state["Class"][top_bracket].inc( top_bracket )

    @staticmethod
    def _generate_new_sese_context( context, pdst_graph ) :
        index = 0

        while (context, index) in pdst_graph.nodes() :
            index += 1

        return context, index

    @staticmethod
    def _build_tree( node, context, cfg_graph, state, pdst : PDST, function : Function) :
        pdst_graph = pdst.get_graph()

        for t in cfg_graph.successors( node ) :
            PDSTBuilder.logger.debug( """Examining edge %s --> %s""" % (str( node ), str( t )) )
            cl = state["Class"][(node, t)]

            if state["capping"][(node, t)] :
                raise Exception( "ASSERT Capping" )

            newContext = context

            if cl is not None and (not cl.is_first() or not cl.is_last()) :
                if cl.is_first() :
                    PDSTBuilder.logger.debug(
                        """[INFO] Entering region w/ class %s and count = %s and context = %s""" % (
                            str( cl ), str( cl.count ), str( context )) )

                    newContext = SESERegion( (cl.number, 0), (node, t), function )

                    pdst_graph.add_node( newContext )
                    pdst_graph.add_edge( context, newContext )

                elif not cl.is_last() :
                    PDSTBuilder.logger.debug( """Moving to adjacent region w/ context %s""" % str( context ) )
                    context.exit = (node, t)

                    previousContext = list( pdst_graph.predecessors( context ) )[0]

                    if previousContext.is_domain() :
                        domain = previousContext
                    else :
                        domain = SESEDomain( cl.number, function )

                        for pred in set( pdst_graph.predecessors( context ) ) :
                            pdst_graph.remove_edge( pred, context )
                            pdst_graph.add_edge( pred, domain )

                        pdst_graph.add_edge( domain, context )
                        domain.add_sese( context )

                    numcl, numse = context.num
                    newContext = SESERegion( (cl.number, numse + 1), (node, t), function )

                    pdst_graph.add_node( newContext )
                    pdst_graph.add_edge( domain, newContext )
                    domain.add_sese( newContext )

                else :
                    PDSTBuilder.logger.debug( """Exiting region w/ context = %s""" % str( context ) )
                    context.exit = (node, t)
                    domain = list( pdst_graph.predecessors( context ) )[0]

                    if domain.is_domain() :
                        parent = list( pdst_graph.predecessors( domain ) )[0]
                    else :
                        parent = domain

                    newContext = parent

                cl.dec()

            if state["dfsnum"].get( t ) is not None :
                state["dfsnum"][t] = None
                PDSTBuilder._build_tree( t, newContext, cfg_graph, state, pdst, function )

            """ TODO : link directly to BB and not BB num """
            # bb = function.cfg.bbs[t]
            #
            # if bb.callTarget != None:
            # 	""" The BB contains a call to another function """
            # 	G = function._program.icfg.get_graph()
            # 	callee = list(filter(lambda t: G[function][t]["callBB"] == bb , G[function]))[0]
            # 	newContext.callingBBs.append()
            newContext.nodes.add( t )
