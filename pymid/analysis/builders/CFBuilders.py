# -*- coding: utf-8 -*-

from typing import List

from pymid.analysis.program import Function, BasicBlock, Program
from pymid.analysis.CFG import CFG, ICFG, CFException
from pymid.utils import loggingConf


class CFGBuilder( object ) :
    alwaysTrue = [ 'b', 'b,n', 'ba', 'ba,a', 'fba', 'fba,a', 'cba', 'cba,a', 'jmp' ]
    alwaysFalse = [ 'bn', 'bn,a', 'fbn', 'fbn,a', 'cbn', 'cbn,a' ]

    logger = loggingConf.getLogger( "CFGBuilder" )

    def __init__( self ) :
        """ Virtually private constructor """

        raise Exception( "The class CFGBuilder is a static only class" )

    @staticmethod
    def build_cfg( function: Function ) -> CFG :
        CFGBuilder.logger.info( """Constructing CFG of %s""" % function.get_name() )

        cfg = CFG( function )

        BB = BasicBlock( start=function.get_address(), end=function.get_address() - 4,
                         function=function )
        delay_slot_instruction = False

        links = [ ]
        targets = list()
        found_ret = False
        first_bb = True

        for instruction in function.get_instructions() :
            BB.add_inst()

            if delay_slot_instruction :
                # This is the last instruction so we add the resulting basic block to the function
                cfg.add_bb( BB )
                CFGBuilder.logger.debug( """Added BB : %s """ % str( BB ) )

                for target in targets :
                    links.append( (BB.end, target) )

                targets = list()

                if found_ret :
                    cfg.link_to_end( BB.end )

                if first_bb :
                    cfg.link_to_start( BB.start )

                delay_slot_instruction = False
                found_ret = False
                first_bb = False
                CFGBuilder.logger.debug( """BB[%d] start : %s""" % (len( cfg.bbs.BBs ), hex( BB.end + 4 )) )
                BB = BasicBlock( start=BB.end + 4, end=BB.end, function=function )
                continue

            if instruction.get_type() in [ 'branch', 'jump' ] :
                #  A branch to treat

                delay_slot_instruction = True

                if type( instruction.get_target() ) == str and instruction.get_target()[ 0 ] == '%' :
                    CFGBuilder.logger.warning(
                        """Unable to determine the target for the following instruction :\n\t%s""" % instruction.get_code() )
                    function.safe = False

                else :
                    target = int( instruction.get_target(), base=16 )
                    if instruction.get_opcode() not in CFGBuilder.alwaysFalse :
                        CFGBuilder.logger.debug(
                            """Creating link based on the following opcode \"%s\"""" % str( instruction.get_opcode() ) )
                        targets.append( target )

                if instruction.get_opcode() not in CFGBuilder.alwaysTrue :
                    target = instruction.get_address() + 8
                    CFGBuilder.logger.debug(
                        """Creating link by fall-through to the following address %s""" % (hex( target )) )
                    targets.append( target )

            elif instruction.get_type() == 'ret' :
                delay_slot_instruction = True
                found_ret = True

            elif instruction.get_type() == 'call' :
                delay_slot_instruction = True

                if type( instruction.get_target() ) == str and instruction.get_target()[ 0 ] == '%' :
                    CFGBuilder.logger.warning(
                        """Unable to determine the target for the following instruction :\n\t%s""" % instruction.get_code() )
                    function.safe = False
                else :
                    try :
                        callTarget = int( instruction.get_target(), base=16 )
                    except Exception :
                        function.safe = False
                        raise Exception( instruction )

                    BB.set_call_target( callTarget )

                target = instruction.get_address() + 8
                CFGBuilder.logger.debug( """Creating link by fall-through to the following address %d""" % target )
                targets.append( target )

        if BB.start <= BB.end :
            # No CF instruction at the end of the function, so we add the last BB and treat it as unsafe
            CFGBuilder.logger.debug( """No control-flow instruction at the end of the last basic block""" )
            cfg.add_bb( BB )
            CFGBuilder.logger.debug( """Added BB : %s """ % str( BB ) )

            for target in targets :
                links.append( (BB.end, target) )

            cfg.link_to_end( BB.end )
            function.safe = False

        CFGBuilder.logger.info( """Generating the graph""" )
        CFGBuilder.logger.debug( """\tlinks : """ )
        for (end, target) in links :
            CFGBuilder.logger.debug( """\t%s\t%s""" % (hex( end ), hex( target )) )

        for (end, target) in links :
            CFGBuilder.logger.debug( """Treating link : %s --> %s """ % (hex( end ), hex( target )) )
            try :
                cfg.add_edge( end, target )
            except CFException as e :
                CFGBuilder.logger.warning( str( e.args ) )
                CFGBuilder.logger.warning( """Maybe we hit the libc ?""" )
                function.safe = False

        function.cfg = cfg
        return cfg


class ICFGBuilder :
    logger = loggingConf.getLogger( "ICFGBuilder" )

    def __init__( self ) :
        """ Virtually private constructor """
        raise Exception( "The class ICFGBuilder is a static only class" )

    @staticmethod
    def build_icfg( program: Program ) -> ICFG :
        ICFGBuilder.logger.info( """Constructing the ICFG of the program %s""" % program.get_name() )

        starting_point: str = program.starting_point
        icfg = ICFG( program )
        program.icfg = icfg

        starting_function = program.get_function_from_name( starting_point )
        stack = [ ]

        ICFGBuilder._analyze_function( starting_function, stack, icfg, program )

        if len( stack ) != 0 :
            ICFGBuilder.logger.warning(
                """ ICFG analysis of function finished with strange stack : %s""" % str( stack ) )

        return icfg

    @staticmethod
    def _analyze_function( function: Function, stack: List[ Function ],
                           icfg: ICFG, program: Program ) :
        if function in stack :
            # We are in presence of a recursive function, we treat each function after its apparition as recursive as well
            index_function = stack.index( function )

            for i in range( index_function, len( stack ) ) :
                stack[ i ].recursive = True
                ICFGBuilder.logger.debug( """Found the following recursive function : %s""" % stack[ i ].get_name() )

        if function in icfg :
            # The function has been/is already treated, return
            ICFGBuilder.logger.debug( """ Quick exit %s """ % function.get_name() )
            return

        ICFGBuilder.logger.debug( """Analyzing function %s""" % function.get_name() )

        stack.append( function )
        icfg.add_function( function )

        for BB in function.cfg.bbs :
            if BB.callTarget is not None :
                target = BB.callTarget
                callee = None
                index = program.funcAddMap.get( target )
                if index is not None :
                    callee = program.functions[ index ]

                if callee is None :
                    ICFGBuilder.logger.warning(
                        """ Unable to find a target function at address %s inside the function %s""" % (
                            hex( target ), function.get_name()) )
                else :
                    ICFGBuilder._analyze_function( callee, stack, icfg, program )
                    icfg.add_call( function, callee, BB )

        stack.pop()
