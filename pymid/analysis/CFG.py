# -*- coding: utf-8 -*-

from pymid.analysis.program import BasicBlock
from pymid.utils import loggingConf
from pymid.utils.graphUtility import GraphClass


class CFException( Exception ) :
    def __init__( self, message ) :
        super().__init__( str( message ) )


class BasicBlockList( object ) :
    """
    A list of basic block with fast retrieving for start and end address
    """

    logger = loggingConf.getLogger( "BBList" )

    def __init__( self ) :
        self.BBs = list()
        self.startMap = dict()
        self.endMap = dict()

    def get_bb_from_start( self, start_add: int ) :
        index = self.get_bb_num_from_start( start_add )
        return self.BBs[index]

    def get_bb_from_end( self, end_add: int ) :
        index = self.get_bb_num_from_end( end_add )
        return self.BBs[index]

    def get_bb_num_from_start( self, start_add: int ) :
        index = self.startMap.get( start_add )
        if index is None or len( self.BBs ) <= index :
            raise CFException( "There is no Basic Block starting at the following address : %s" % hex( start_add ) )

        return index

    def get_bb_num_from_end( self, end_add: int ) :
        index = self.endMap.get( end_add )
        if index is None or len( self.BBs ) <= index :
            raise CFException( "There is no Basic Block ending at the following address : %s" % hex( end_add ) )

        return index

    def add_bb( self, bb: BasicBlock ) :
        index = len( self.BBs )

        self.startMap[bb.start] = index
        self.endMap[bb.end] = index

        self.BBs.append( bb )

    def get_bb_num( self, add ) :
        for i in range( len( self.BBs ) ) :
            if add in self.BBs[i] :
                return i

        return -1

    def split( self, split_num, split_add, split_add_to_new_bb=True ) :
        """
        Split a basic block designated by a 'splitNum' at an address 'splitAdd'
        """
        if len( self.BBs ) <= split_num :
            raise CFException( "The Basic Block to split does not exist" )
        if split_add not in self.BBs[split_num] :
            raise CFException( "The Basic Block to split does not contain the splitting address" )

        if split_add_to_new_bb :
            return self._split_intern( split_num, split_add )
        else :
            return self._split_intern( split_num,
                                       split_add + 4 )  # The factor is due to the constant size of Leon3 instructions

    def _split_intern( self, split_num, split_add ) :
        self.logger.debug( """ Splitting a BasicBlock at the address : %s """ % hex( split_add ) )

        previousBB = self.BBs[split_num]

        if split_add == previousBB.start :
            raise CFException( "Impossible to split a basic block at its starting address" )

        newBB = BasicBlock( split_add, previousBB.end )
        previousBB.end = split_add - 4
        newBB.callTarget = previousBB.callTarget
        previousBB.callTarget = None

        indexNewBB = len( self.BBs )

        self.BBs.append( newBB )

        self.startMap[newBB.start] = indexNewBB
        self.endMap[newBB.end] = indexNewBB
        self.endMap[previousBB.end] = split_num

        return indexNewBB

    def __iter__( self ) :
        return self.BBs.__iter__()


class CFG( GraphClass ) :
    logger = loggingConf.getLogger( "CFG" )

    def __init__( self, function ) :
        super().__init__( "DiGraph" )

        self._function = function

        self.bbs = BasicBlockList()

        self.graph.add_node( 'start' )
        self.graph.add_node( 'end' )

    def link_to_start( self, add: int ) :
        n = self.bbs.get_bb_num_from_start( add )
        self.graph.add_edge( 'start', n )

    def link_to_end( self, add: int ) :
        n = self.bbs.get_bb_num_from_end( add )
        self.graph.add_edge( n, 'end' )

    def add_bb( self, bb, **att ) :
        self.bbs.add_bb( bb )
        bb._function = self.function

        n = self.bbs.get_bb_num_from_start( bb.start )
        self.graph.add_node( n, **att )

    def add_edge( self, end, target, **att ) :
        try :
            e1 = self.bbs.get_bb_num_from_end( end )
        except CFException :
            self.logger.debug( """e1 not found, running fallback""" )
            # The address is not the ending point of a BB, we search for
            # the basic block containing the address
            index = self.bbs.get_bb_num( end )

            if index == -1 :
                raise CFException( "An error occurred: unable to find a basic block with the end address" )

            self.split_bb( index, end, False )

            # We update e1
            e1 = self.bbs.get_bb_num_from_end( end )

        try :
            e2 = self.bbs.get_bb_num_from_start( target )

        except CFException :
            self.logger.debug( """e2 not found, running fallback""" )
            # The address is not the starting point of a BB, we search for
            # the basic block containing the address
            index = self.bbs.get_bb_num( target )

            if index == -1 :
                raise CFException( "An error occurred: unable to find a basic block with the target address" )

            self.split_bb( index, target, True )

            # We update e2 and e1 as we could have modify it with the split
            e2 = self.bbs.get_bb_num_from_start( target )
            e1 = self.bbs.get_bb_num_from_end( end )

        self.graph.add_edge( e1, e2, **att )

    def split_bb( self, bb_num, split_add, split_add_to_new_bb ) :
        # Split the basic block at the split address and create a new BB beginning at the split address
        self.logger.debug( """ Splitting a BasicBlock at the address : %s """ % hex( split_add ) )

        newBBNum = self.bbs.split( bb_num, split_add, split_add_to_new_bb )

        successorBB = set( self.graph.successors( bb_num ) )

        # Update the CFG
        for i in successorBB :
            self.graph.remove_edge( bb_num, i )
            self.graph.add_edge( newBBNum, i )

        self.graph.add_edge( bb_num, newBBNum )

    @property
    def function( self ):
        return self._function

    def __str__( self ) :
        return self.__repr__()

    def __repr__( self ) :
        return '<CFG \'' + self._function.get_name() + '\'>'


class ICFG( GraphClass ) :
    """
    Links between the functions of a program
    """

    def __init__( self, program ) :
        super().__init__( "MultiDiGraph" )
        self._program = program

    def add_function( self, f ) :
        self.graph.add_node( f )

    def add_call( self, caller, callee, bb ) :
        self.graph.add_edge( caller, callee, callBB=bb )
