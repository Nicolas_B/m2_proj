"""
Parse the binary by using objdump

Works for :
    Leon3 architecture
"""

# -*- coding: utf-8 -*-

import re
import io

from pymid.analysis.program import Instruction, Function
from pymid.utils import loggingConf


class InstructionReader( object ) :
    """
    Interface for classes that reads the instructions from a binary.
    Its role is to separate the different instructions and to sort them
    inside functions as well as identify them (branch, ret, other).
    """

    @staticmethod
    def read_stream( stream ) :
        None

    @staticmethod
    def read_string( string: str ) :
        None


class Leon3FromObjdump( InstructionReader ) :
    """
    Singleton object able to provide an organized stream of leon3 instruction from objdump result
    """

    branchRe = re.compile( '^(?P<target>(([0-9a-f]{1,8})|(%[a-z][0-9]+)))' )
    callRe = branchRe
    jumpRe = branchRe

    branchOnIntegerOpcode = ['b', 'ba', 'bn', 'bne', 'be', 'bg', 'ble', 'bge', 'bl', 'bgu', 'bleu', 'bcc', 'bcs',
                             'bpos', 'bneg', 'bvc', 'bvs']
    branchOnIntegerFullOpcode = branchOnIntegerOpcode + [i + ',a' for i in branchOnIntegerOpcode]

    branchOnFloatOpcode = ['fba', 'fbn', 'fbu', 'fbg', 'fbug', 'fbl', 'fbul', 'fblg', 'fbne', 'fbe', 'fbue', 'fbge',
                           'fbuge', 'fble', 'fbule', 'fbo']
    branchOnFloatFullOpcode = branchOnFloatOpcode + [i + ',a' for i in branchOnFloatOpcode]

    branchOnCoProcessorOpcode = ['cba', 'cbn', 'cb3', 'cb2', 'cb23', 'cb1', 'cb13', 'cb12', 'cb123', 'cb0', 'cb03',
                                 'cb02', 'cb023', 'cb01', 'cb013', 'cb012']
    branchOnCoProcessorFullOpcode = branchOnCoProcessorOpcode + [i + ',a' for i in branchOnCoProcessorOpcode]

    branchOpcode = branchOnIntegerFullOpcode + branchOnFloatFullOpcode + branchOnCoProcessorFullOpcode
    callOpcode = ['call']
    jumpOpcode = ['jmp']
    retOpcode = ['ret', 'retl']

    instructionProcess = {
        'branch' : {
            're' : lambda x : Leon3FromObjdump.branchRe.match( x.strip() ),
            'opcodes' : branchOpcode,
        },
        'call' : {
            're' : lambda x : Leon3FromObjdump.callRe.match( x.strip() ),
            'opcodes' : callOpcode,
        },
        'jump' : {
            're' : lambda x : Leon3FromObjdump.jumpRe.match( x.strip() ),
            'opcodes' : jumpOpcode,
        },
        'ret' : {
            're' : lambda x : None,
            'opcodes' : retOpcode,
        },
    }

    functionHeaderRe = re.compile( "^(?P<add>[0-9a-fA-F]{,8}) <(?P<name>[^ >]*)>:" )
    instructionRe = re.compile(
        "^(?P<add>[0-9a-fA-F]{,8}):\t(?P<binary>([0-9a-fA-F]{2} ){4})\t(?P<opcode>[^ ]*)( |\t)*(?P<operand>.*)$" )

    logger = loggingConf.getLogger( "InstructionReader" )

    def __init__( self ) :
        """ Virtually private constructor """

        raise Exception( "The class Leon3FromObjdump is a static only class" )

    @staticmethod
    def read_stream( stream ) :
        currentFunction = None

        for line in stream :
            line = line.strip()
            Leon3FromObjdump.logger.debug( """Analyzing the following line : \"%s\" """ % line )
            mF = Leon3FromObjdump.functionHeaderRe.match( line )

            if mF is not None :
                Leon3FromObjdump.logger.debug( """    Found the following function : %s """ % mF.group( "name" ) )
                currentFunction = Function( mF.group( "name" ), int( mF.group( "add" ), 16 ), program=None )
                continue

            mI = Leon3FromObjdump.instructionRe.match( line )

            if mI is not None :
                if currentFunction is None :
                    raise Exception( "Found instruction without function : %s" % line )
                else :
                    Leon3FromObjdump.logger.debug( """    Sorted as instruction """ )
                    instruction = Leon3FromObjdump._read_instruction( mI )
                    currentFunction.add_instruction( instruction )

            if line == "" and currentFunction is not None :
                yield currentFunction

    @staticmethod
    def read_string( string: str ) :
        return Leon3FromObjdump.read_stream( io.StringIO( string ) )

    @staticmethod
    def _read_instruction( meta_instruction ) :
        add = int( meta_instruction.group( "add" ), base=16 )
        code = meta_instruction.group()
        curT = 'other'
        target = None
        opcode = meta_instruction.group( 'opcode' )

        for t, process in Leon3FromObjdump.instructionProcess.items() :
            if meta_instruction.group( 'opcode' ) in process['opcodes'] :
                curT = t
                m = process['re']( meta_instruction.group( "operand" ) )
                if m is not None :
                    target = m.group( 'target' )
                break

        return Instruction( curT, code, add, opcode, target )
