# -*- coding: utf-8 -*-
import pathlib
import typing

import pymid.aiT.ais
from pymid.utils import loggingConf


class Instruction( object ) :
    """
    An interface for every instructions
    """

    instructionTypes = ['unknown', 'branch', 'call', 'jump', 'ret', 'other']

    def __init__( self, inst_type='unknown', code=None, address=None, opcode=None, target=None ) :
        if inst_type not in self.instructionTypes :
            raise Exception( "Instruction type not correct : %s" % str( inst_type ) )

        self._type = inst_type
        self._code = code
        self._target = target
        self._address = address
        self._opcode = opcode

    def get_type( self ) : return self._type

    def get_code( self ) : return self._code

    def get_target( self ) : return self._target

    def get_address( self ) : return self._address

    def get_opcode( self ) : return self._opcode


class BasicBlock( object ) :
    """
    Define the basic blocks of a program
    """

    def __init__( self, start=None, end=None, function=None ) :
        self.start = start
        self.end = end
        self.callTarget = None
        self._function = function

    def set_call_target( self, add: int ) : self.callTarget = add

    def add_inst( self ) : self.end += 4

    def __len__( self ) :
        return ((self.end - self.start) // 4) + 1

    def __contains__( self, add ) :
        if add % 4 != 0 :
            raise Exception( " The instruction address is not a multiple of 4 ", self )
        return self.start <= add <= self.end

    def __str__( self ) :
        return self.__repr__()

    def __repr__( self ) :
        return '<BB start :' + hex( self.start ) + ', end :' + hex( self.end ) + '>'


class Function( object ) :
    """
    An interface regrouping every information we have on a function
    """

    def __init__( self, name: str, start: int, program ) :
        self.name = name
        self.safe = True
        self.recursive = False
        self.start = start  # Could be just the first  instruction stocked
        self.instructions = list()
        self.cfg = None
        self.pdst = None
        self._program = program

    """ Getters """

    def get_name( self ) -> str :
        return self.name

    def is_safe( self ) -> bool :
        return self.safe

    def is_recursive( self ) -> bool :
        return self.recursive

    def get_address( self ) -> int :
        return self.start

    def get_instructions( self ) -> typing.List[Instruction] :
        return self.instructions

    def get_cfg( self ) :
        return self.cfg

    def get_pdst( self ) :
        return self.pdst

    def get_start( self ) -> int :
        return self.start

    @property
    def program( self ) :
        return self._program

    """ End Getters """

    def add_instruction( self, inst: Instruction ) :
        self.instructions.append( inst )

    def get_instruction_by_add( self, add: int ) -> Instruction :
        """
        Recover the instruction inside this function at a given address
        First tries to compute the index of the instruction, if failed then iter among the function's instruction
        to find it
        """
        index: int = (self.start - add) // 4
        inst: Instruction = self.instructions[index]

        if inst.get_address() != add :
            i: Instruction
            for i in self.instructions :
                if i.get_address() == add :
                    inst = i
                    break

        # Assure that we retrieve the right address
        if inst.get_address() != add :
            raise Exception(
                "Impossible to find instruction at address %d in the function %s" % (add, str( self.name )) )

        return inst

    def __str__( self ) :
        return self.__repr__()

    def __repr__( self ) :
        return '<Function \'' + self.name + '\'>'


class Program( object ) :
    """
    Represent the information of a whole program
    """

    logger = loggingConf.getLogger( "Program" )

    def __init__( self, name: str, binary_path: pathlib.Path, starting_point: str ) :
        self.name = Program._sanitize( name )
        self.binaryPath = binary_path
        self.starting_point = starting_point

        if not self.binaryPath.exists() :
            raise Exception( """ Binary %s not present """ % str( self.binaryPath ) )

        self.functions = list()
        self.funcAddMap = dict()
        self.funcNameMap = dict()
        self.icfg = None
        self.ipdst = None
        self.labelIndependantHeader: typing.Optional[pymid.aiT.ais.AisFile] = None

    @staticmethod
    def _sanitize( name ) :
        return name.split( '.' )[0].split( ' ' )[0]

    def __iter__( self ) :
        return self.functions.__iter__()

    def get_name( self ) -> str :
        return self.name

    def add_function( self, f: Function ) :
        index: int = len( self.functions )
        self.functions.append( f )
        self.funcAddMap[f.get_start()] = index
        self.funcNameMap[f.get_name()] = index
        f._program = self

    def get_function_from_name( self, name: str ) -> Function :
        func_index = self.funcNameMap.get( name )

        if func_index is None :
            raise Exception( "The function \"%s\" is not present in the program \"%s\"" % (name, self.name) )

        return self.functions[func_index]
