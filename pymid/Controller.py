#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse as ap
import subprocess as sp
import importlib.util
import pathlib
import os
from networkx.algorithms.dag import topological_sort
from itertools import combinations

import pymid.analysis.PDST
#  from pymid.analysis.SeseSelection.CFGExpansion import expanse_cfg
from pymid.analysis.SeseSelection.Core import SeseSelectionSolver, SelectedGraph
from pymid.analysis.SeseSelection.Watcher import Watcher
from pymid.analysis.SeseSelection.localWCET import localWcetSeseSelector, SimpleMemoryModel
from pymid.analysis.binaryParser import Leon3FromObjdump
from pymid.analysis.builders.CFBuilders import CFGBuilder, ICFGBuilder
from pymid.analysis.builders.PDSTBuilder import PDSTBuilder
from pymid.analysis.predicats import is_secured
from pymid.analysis.program import Program, Function
from pymid.utils import loggingConf
from pymid.utils.graphUtility import print_dot_pdst, dfs_conditional_stop

logger = loggingConf.getLogger( "Controller" )


class Project( object ) :
    logger = loggingConf.getLogger( "Project" )

    parser = ap.ArgumentParser()
    parser.add_argument( "startupFile", help="The file used to define the parameters", type=pathlib.Path )
    parser.add_argument( "--graph", help="Display the pdst and pdst superposed on the cfg of the program",
                         action='store_true' )
    parser.add_argument( "--expand_cfg",
                         help="Expand the cfg to generate new SESE from nodes with multiple exit or entry",
                         action='store_true' )
    parser.add_argument( "--only-info",
                         help="Perform the program analysis and store the data about the number of regions in a provided file",
                         action="store_true" )

    def __init__( self ) :
        self.args = self.parser.parse_args()
        spec = importlib.util.spec_from_file_location( "startup", self.args.startupFile )
        foo = importlib.util.module_from_spec( spec )
        spec.loader.exec_module( foo )
        startup = foo.startup
        self.programs = list()

        self._analyze_startup( startup )

    def _analyze_startup( self, startup ) :
        self.options = startup.get( "options", [ ] )
        self.objdump_path = startup[ "objdump" ]
        self.memory_space = startup.get( "memory_space", None )
        self.stack_space = startup.get( "stack_space", None )
        self.max_subSese = startup.get( "max_subSese", None )
        self.WCETSolverInterface = startup[ "WCETSolverInterface" ]( self )

        self.tmp_dir = pathlib.Path( startup.get( "tmp_dir", "./.tmp" ) )
        self.results_dir = pathlib.Path( startup.get( "results_dir", "./results" ) )
        self.program_dir = pathlib.Path( startup.get( "program_dir", "." ) )
        self.resulting_data_file = self.results_dir.joinpath( startup.get("resulting_data_file", "result.data") )

        self.logger.info( """ Creating the project directories """ )
        self.tmp_dir.mkdir( parents=True, exist_ok=True )
        self.results_dir.mkdir( parents=True, exist_ok=True )

        self.logger.info( """ Generating the program interfaces """ )

        program_name: str
        for program_name in startup[ "programs" ] :
            program: Program = Program( program_name, self.program_dir.joinpath( program_name ),
                                        startup[ "programs" ][ program_name ][ "starting_point" ] )
            self.WCETSolverInterface.init_program( startup[ "programs" ][ program_name ], program )
            program.results_dir = pathlib.Path(
                startup[ "programs" ][ program_name ].get( "results_dir",
                                                           self.results_dir.joinpath(
                                                               program_name.split( "." )[ 0 ] ) ) )
            program.results_dir.mkdir( parents=True, exist_ok=True )
            self.programs.append( program )

    def analyse_programs( self ) :
        binary_reader = Leon3FromObjdump

        program: Program
        for program in self.programs :
            self.logger.info( """ Analyse the binary %s """ % program.get_name() )

            p = sp.run( [ str( self.objdump_path ), "-d", str( program.binaryPath ) ], stdout=sp.PIPE )
            p.stdout = p.stdout.decode( 'utf-8' )

            func: Function
            for func in binary_reader.read_string( p.stdout ) :
                program.add_function( func )

            self._build_control_flow( program )

            # if self.args.expand_cfg :
            #     """ Execute the sub basic block splitting of the cfg """
            #     for func in filter( is_secured, program.icfg ) :
            #         expanse_cfg( func.cfg )

            self._build_structure_tree( program )

    def _build_control_flow( self, program: Program ) :
        """
        Build information about the Control-Flow of the program (CFG + ICFG)
        """
        cfg_builder = CFGBuilder
        icfg_builder = ICFGBuilder

        self.logger.info( """ Build the Control-Flow """ )

        func: Function
        for func in program.functions :
            func.cfg = cfg_builder.build_cfg( func )

        program.icfg = icfg_builder.build_icfg( program )

    def _build_structure_tree( self, program: Program ) :
        """
        Build information about the Structure Tree (PDST + SESE regions)
        """

        self.logger.info( """ Built the Structure Tree """ )

        pdst_builder = PDSTBuilder
        func: Function

        for func in filter( is_secured, program.icfg ) :
            self.logger.info( """Recovering the SESE regions and PDST of %s""" % func.get_name() )
            func.pdst = pdst_builder.build_pdst( func )

        if self.args.expand_cfg :
            for func in filter( is_secured, program.icfg ) :
                self.WCETSolverInterface.sese_expansion( func )

        program.ipdst = pymid.analysis.PDST.IPDST( program )


if __name__ == '__main__' :
    project = Project()
    project.analyse_programs()

    if project.args.graph :
        """ Place multiple graphs in a specified directory and exit """
        for P in project.programs :
            cur_dir = pathlib.Path( "." ).absolute()
            os.chdir( str( P.results_dir ) )
            P.icfg.print( "%s_icfg" % P.name )

            for function in filter( is_secured, P.icfg ) :
                function.pdst.print( "%s_%s_pdst" % (P.name, function.name) )
                print_dot_pdst( function.cfg.get_graph(), function.pdst, "%s_%s_cfgOnPdst" % (P.name, function.name) )

            os.chdir( str( cur_dir ) )
        exit( 0 )

    if project.args.only_info :
        file_path = project.results_dir.joinpath( "init_only.csv" )
        write_header = not file_path.exists()

        numberOfSese = 0
        for P in project.programs :
            for _ in filter( lambda s : s.is_region(), P.ipdst ) :
                numberOfSese += 1

        basic_block_number = 0
        for P in project.programs :
            for function in filter( is_secured, P.icfg ) :
                basic_block_number += len( function.cfg.graph.nodes ) - 2

        with project.results_dir.joinpath( "init_only.csv" ).open( "a" ) as f :
            if write_header :
                f.write( "name;number all seses;number basic block\n" )
            f.write( "%s;%d;%d\n" % (
                project.programs[ 0 ].name,
                numberOfSese,
                basic_block_number
            ) )
        exit( 0 )

    watcher = Watcher( project.programs[ 0 ].results_dir, 'step_by_step_mid' )
    memoryModel = SimpleMemoryModel( project )
    selector = localWcetSeseSelector( project, memoryModel )
    solver = SeseSelectionSolver( project, memoryModel, selector, watcher )

    logger.info( """ Solving the sese problem """ )

    solver.solve()

    numberOfSese = 0

    for P in project.programs :
        for function in filter( is_secured, P.icfg ) :
            numberOfSese += len( function.get_pdst().get_graph().nodes ) - 1

    selected_graph = SelectedGraph( solver )
    extra_memory: int = selected_graph.get_extra_memory()

    if not project.results_dir.joinpath( "data_exp.csv" ).exists() :
        with project.results_dir.joinpath( "data_exp.csv" ).open( "a" ) as f :
            f.write(
                "name;solving time;MIT time;MIT call;number all seses;number selected seses;final goal;extra memory\n" )

    with project.results_dir.joinpath( "data_exp.csv" ).open( "a" ) as f :
        f.write( "%s;%f;%f;%d;%d;%d;%d;%d\n" % (
            project.programs[ 0 ].name,
            solver.timeSolving, project.WCETSolverInterface.timeMIT, project.WCETSolverInterface.MITCall, numberOfSese,
            len( solver.selected ), solver.goal, extra_memory) )

    logger.info( """GOAL : %s""" % solver.goal )
    logger.info( """Results : %s """ % str( { x : (x.WCET_OOC, x.WCET_OOC_update) for x in solver.selected } ) )

    logger.info( """ *** Final data *** """ )

    P = project.programs[0]

    ipdst = P.ipdst.get_graph()

    sorted_nodes = list( filter( lambda s : s.selected, topological_sort( ipdst ) ) )

    shift_address = {}
    selected_descendants = {}

    for (s1,s2) in combinations(sorted_nodes, 2):
        if s1.metadata.startAdd == s2.metadata.startAdd:
            print(""" %s start address conflict with %s """ % (str(s1), str(s2)))
            if s1.is_domain() and s2.is_region():
                shift_address[s2] = (4, shift_address.get(s2, (0,0))[1])
            elif s2.is_domain() and s1.is_region():
                shift_address[s1] = (4, shift_address.get(s1, (0,0))[1])
            else:
                print(" Impossible to perform the shift of address for %s and %s" % (str(s1), str(s2)))

        if s1.metadata.endAdd == s2.metadata.endAdd:
            print(""" %s end address conflict with %s """ % (str(s1), str(s2)))
            if s1.is_domain() and s2.is_region():
                shift_address[s2] = (shift_address.get(s2, (0,0))[0], -4)
            elif s2.is_domain() and s1.is_region():
                shift_address[s1] = (shift_address.get(s1, (0,0))[0], -4)
            else:
                print(" Impossible to perform the shift of address for %s and %s" % (str(s1), str(s2)))

    print(shift_address)

    max_descendants = 8

    for sese in sorted_nodes:
        def descendant_predicat( sel ) :
            if sel == sese:
                return False
            else:
                return sel.selected

        direct_descendants = list( dfs_conditional_stop( ipdst, sese, descendant_predicat, descendant_predicat ) )
        selected_descendants[ sese ] = direct_descendants
        max_descendants = max( max_descendants, len(direct_descendants) )

    with open(project.resulting_data_file, "w+") as final_result_file:
        for i, sese in enumerate(sorted_nodes):
            descendants = [ 0 for j in range(max_descendants) ]

            for k, n in enumerate( selected_descendants[ sese ] ):
                descendants[k] = sorted_nodes.index( n )
            # descendants = [ sorted_nodes.index( n ) for n in selected_descendants[ sese ] ]


            final_result_file.write( """%d, %d, %d, %s, %s, %s\n""" % ( #str(sese),
                                                    i,
                                                   sese.WCET_OOC,
                                                   1 if sese.metadata.forward else 0,
                                                   hex( sese.metadata.startAdd + shift_address.get(sese, (0, 0))[0] ),
                                                   hex( sese.metadata.endAdd + shift_address.get(sese, (0, 0))[1] ),
                                                   str( descendants )) )
