# -*- coding: utf-8 -*-

from logging import getLogger
import logging.config

DEFAULT_LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'consoleFormatter': {
            'format': '[%(levelname)s] [%(name)s] %(message)s'
        }
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'consoleFormatter',
        }
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'INFO',
        },
        'AisFile': {
            'handlers': ['console'],
            'level': 'CRITICAL',
            'propagate': False,
        },
        'AisReader': {
            'handlers': ['console'],
            'level': 'WARNING',
            'propagate': False,
        },
        'AiTXmlReader': {
            'handlers': ['console'],
            'level': 'WARNING',
            'propagate': False,
        },
        'ApxFile': {
            'handlers': ['console'],
            'level': 'CRITICAL',
            'propagate': False,
        },
        'BBList': {
            'handlers': ['console'],
            'level': 'WARNING',
            'propagate': False,
        },
        'CFG': {
            'handlers': ['console'],
            'level': 'WARNING',
            'propagate': False,
        },
        'CFGBuilder': {
            'handlers': ['console'],
            'level': 'CRITICAL',
            'propagate': False,
        },
        'Controller': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': False,
        },
        'ICFGBuilder': {
            'handlers': ['console'],
            'level': 'WARNING',
            'propagate': False,
        },
        'InstructionReader': {
            'handlers': ['console'],
            'level': 'WARNING',
            'propagate': False,
        },
        'localWcetSeseSelector': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': False,
        },
        'PDSTBuilder': {
            'handlers': ['console'],
            'level': 'WARNING',
            'propagate': False,
        },
        'Program': {
            'handlers': ['console'],
            'level': 'WARNING',
            'propagate': False,
        },
        'Project': {
            'handlers': ['console'],
            'level': 'WARNING',
            'propagate': False,
        },
        'run_test': {
            'handlers': ['console'],
            'level': 'WARNING',
            'propagate': False,
        },
        'Solver': {
            'handlers': ['console'],
            'level': 'WARNING',
            'propagate': False,
        },
        'AiTWCETInterface': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': False,
        },
    }
}

logging.config.dictConfig(DEFAULT_LOGGING)
