# -*- coding: utf-8 -*-

import graphviz as gvz
import networkx as nx


def extract_node_from_tree( tree, node ) :
    """
    Remove a node from a tree and recreate the connexion preserve the tree property
    """
    if not tree.is_directed() or not nx.algorithms.tree.is_tree( tree ) :
        raise Exception( "The graph %s is not directed or is not a tree" % str( tree ) )

    if node not in tree :
        raise Exception( "The node %s not in graph %s" % (str( node ), str( tree )) )

    predecessors_list = list( tree.predecessors( node ) )
    successors = tree.successors( node )

    if len( predecessors_list ) != 1 :
        raise Exception( "Trying to remove the root node of the graph %s" % str( tree ) )

    predecessor = predecessors_list[0]
    tree.remove_node( node )

    tree.add_edges_from( [(predecessor, succ) for succ in successors] )


def print_graph( graph, filename="graph", cleanup=True ) :
    # Only use the property of the graph, directly working on a networkx graph
    dot = gvz.Digraph()
    dot.format = 'pdf'

    for node in graph.nodes :
        dot.node( str( node ), str( node ) )

    if graph.is_multigraph() :
        for s, t, _ in graph.edges :
            dot.edge( str( s ), str( t ) )
    else :
        for s, t in graph.edges :
            dot.edge( str( s ), str( t ) )

    dot.render( filename, cleanup=cleanup )


def print_dot_pdst( cfg, pdst, filename="pdst", cleanup=True ) :
    # Work with pdst : PDST
    color_list = ('blueviolet', 'brown', 'cadetblue', 'chartreuse', 'chocolate', 'aquamarine', 'cornflowerblue',
                  'crimson', 'cyan', 'darkgoldenrod1', 'darkorange', 'deeppink', 'darkturquoise', 'forestgreen')
    index = 0

    pdst_graph = pdst.get_graph()

    def recursion( current_subgraph, current_sese ) :
        nonlocal index, color_list, pdst_graph

        color_index = index
        restricted_nodes = current_sese.nodes

        for n in restricted_nodes :
            current_subgraph.node( str( n ) )

        for sese in pdst_graph.neighbors( current_sese ) :
            if sese.is_domain() :
                for region in pdst_graph.neighbors( sese ) :
                    with current_subgraph.subgraph( name='cluster' + str( region.num ),
                                                    graph_attr={ 'shape' : 'box',
                                                                 'color' :
                                                                     color_list[color_index % len( color_list )] }
                                                    ) as c :
                        index += 1
                        recursion( c, region )
            else :
                with current_subgraph.subgraph( name='cluster' + str( sese.num ),
                                                graph_attr={ 'shape' : 'box',
                                                             'color' :
                                                                 color_list[color_index % len( color_list )] }
                                                ) as c :
                    index += 1
                    recursion( c, sese )

    dot = gvz.Digraph( name='example' )

    recursion( dot, pdst.get_root() )

    for s, t in cfg.edges :
        dot.edge( str( s ), str( t ) )

    dot.render( filename, cleanup=cleanup )

def dfs_conditional_stop( graph, source, condition, stop_condition ) :
    visited = set()
    stack = [ source ]

    while len(stack) > 0:
        node = stack.pop()

        if node not in graph.nodes :
            raise Exception( "node %s not in graph %s during dfs" % (str( node ), str( graph )) )

        if node in visited:
            continue

        visited.add( node )

        if condition( node ):
            yield node

        if stop_condition( node ):
            continue

        for v in graph.successors( node ):
            stack.append( v )

class GraphClass( object ) :
    """
    An abstract base class that allows to factorize the basic utility of graph-based class
    """

    def __init__( self, graph_type: str ) :
        self.graph = nx.__getattribute__( graph_type )()

    def get_graph( self ) :
        return self.graph

    def get_full_children( self, node, predicat=lambda nn : True ) :
        children = []

        def dfs( n ) :
            nonlocal children

            for child in self.graph.successors( n ) :
                if child == node or child in children or not predicat( node ) :
                    continue
                children.append( child )
                dfs( child )

        dfs( node )
        return children

    def print( self, filename="graph", cleanup=True ) :
        print_graph( self.graph, filename, cleanup )

    def __iter__( self ) :
        return self.graph.nodes().__iter__()

    def __contains__( self, n ) :
        return n in self.graph.nodes
