# -*- coding: utf-8 -*-

# TODO : Fix or remove this file
from typing import Set, Any

import networkx as nx

if __name__ == "__main__":
    G = nx.DiGraph()
    G.add_nodes_from(range(11))
    G.add_nodes_from(['start', 'end'])

    G.add_edges_from(
        [('start', 0), (0, 8), (1, 8), (8, 1), (8, 2), (2, 9), (9, 3), (3, 9), (9, 4), (4, 10), (10, 6), (6, 5),
         (5, 10), (6, 7), (7, 'end')])

    SESE = nx.DiGraph()
    SESE.add_node('root')
    SESE.add_nodes_from([(0, i) for i in range(7)])
    SESE.add_nodes_from([(i, 0) for i in range(1, 4)])
    SESE.add_edges_from([('root', (0, i)) for i in range(7)])
    SESE.add_edges_from([((0, 1), (1, 0)), ((0, 3), (2, 0)), ((0, 5), (3, 0))])

    # noinspection PyTypeChecker
    SESE.nodes['root']['nodes']: Set[Any] = set([i for i in range(11)] + ['start', 'end'])
    SESE.nodes[(0, 0)]['nodes'] = { 0 }
    SESE.nodes[(0, 1)]['nodes'] = { 8, 1 }
    SESE.nodes[(0, 2)]['nodes'] = { 2 }
    SESE.nodes[(0, 3)]['nodes'] = { 3, 9 }
    SESE.nodes[(0, 4)]['nodes'] = { 4 }
    SESE.nodes[(0, 5)]['nodes'] = { 5, 6, 10 }
    SESE.nodes[(0, 6)]['nodes'] = { 7 }
    SESE.nodes[(1, 0)]['nodes'] = { 1 }
    SESE.nodes[(2, 0)]['nodes'] = { 3 }
    SESE.nodes[(3, 0)]['nodes'] = { 5 }

    # p = generate_dot(G, SESE)
    # p.format = 'pdf'
    # p.view(cleanup=True)
