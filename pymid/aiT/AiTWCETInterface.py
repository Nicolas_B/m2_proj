# -*- coding: utf-8 -*-

import collections
import time
import pathlib
from functools import reduce
from typing import List, Optional, Dict, Union

import networkx as nx

from pymid.aiT.aiTAnalyses import aiTWcetAnalysis
from pymid.aiT.aiTProject import aiTProject
from pymid.aiT.reader import AisReader
from pymid.aiT.ais import AisLoop, AisFile, AisTryZone, AisMapping, AisInclude, AisSuppress, AisLabel, AisEvaluateZone, \
    AisSnippetNotAnalyzed, AisUnknown, AisLoopTiming
from pymid.aiT.reader import AiTXmlReader
from pymid.analysis.predicats import is_secured, secured_function_iter
from pymid.utils import loggingConf
from pymid.analysis.program import Program, Function, BasicBlock
from pymid.analysis.PDST import SesePart, SESERegion, SESEDomain
from pymid.utils.graphUtility import dfs_conditional_stop
from pymid.aiT.aiTAnalyses import aiTCfgAnalysis

SeseAisMetadata = collections.namedtuple( 'SeseAisMetadata', 'startAdd endAdd id stopDecodingAdds hitTheEnd forward' )


class AiTWCETInterface( object ) :

    def __init__( self, project ) :
        self.aisSeseHeaders = dict()
        self.genericAisCounter = 0
        self.project = project
        self.updateBase = True
        self.seseMapping = dict()
        self.timeMIT = 0
        self.MITCall = 0
        self.aiTprojects = { }
        self.logger = loggingConf.getLogger( "AiTWCETInterface" )

    @staticmethod
    def init_program( program_options, program: Program ) -> None :
        program.ais_path = pathlib.Path( program_options[ "ais" ] )

        if not program.ais_path.exists() :
            raise Exception( "Ais file %s does not exists" % str( program.ais_path ) )

    def pre_hook( self ) -> None :
        """
        Performs the operations required to start the analysis, adapt the graphs to aiT way of work, ...
        """

        program: Program
        for program in self.project.programs :
            self.logger.info( "Recovering CFGs from AiT and gathering information" )

            analysis_name: str = "CFG_%s" % program.name
            self._perform_ait_cfg_analysis( program, analysis_name )

            # Step 4 : Perform full SESE selection
            # Step 4.1 : Read the Annotation file provided and generate a header from it

            self.logger.info( "Generating label independent header" )
            ais_data = AisReader.analyze_ais( program.ais_path )

            cfg_data = self.aiTprojects[ analysis_name ].results[ analysis_name ][ 'xml' ].results[ 'loop_extraction' ]

            # Update the address of each loop
            for inst in ais_data.data :
                if type( inst ) == AisLoop :
                    loop_name = inst.label
                    inst.address = int( cfg_data[ loop_name ][ 'start' ], base=16 )

            # Put everything in a try_block
            new_ais_file = AisFile( program.results_dir.absolute(), "labelIndependentHeader_%s" % program.name )
            program.labelIndependantHeader = new_ais_file
            try_block = AisTryZone()
            try_block.insideInst = ais_data.data.copy()
            new_ais_file.data = [ try_block ]
            new_ais_file.write()

            self.seseMapping[ program ] = dict()

            for function in filter( is_secured, program.icfg ) :
                for sese in filter( lambda x : not x.is_root(), function.get_pdst() ) :
                    self.seseMapping[ program ][ str( sese ) ] = sese

            self._adapt_seses( program )

    def _sese_expansion_extraction( self, sese: SESERegion ) -> List[ int ] :
        """ Detect the basic blocks that are not starting or ending the region and inside the
            region without being in a sub sese.
            Group them in a list and remove them from the sese nodes as they will be used for new subSeses.
         """

        if not sese.is_region():
            raise Exception(" Trying to expanse a non sese region ")

        entry_node = sese.get_entry_edge()[ 1 ]
        exit_node = sese.get_exit_edge()[ 0 ]

        #  Condition to be a loop:
        #  Entry node with multiple entry as we are in presence of only sese regions

        extracted: List[ int ] = [ ]

        predecessors_entry = list(sese.function.cfg.graph.predecessors( entry_node ))
        if len( predecessors_entry ) != 1 :
            # Its a loop
            extracted = list( sese.nodes )
            sese.nodes = set()

        else :
            extracted = list( filter( lambda n : n != entry_node and n != exit_node, sese.nodes ) )
            sese.nodes = { entry_node, exit_node }

        return extracted

    def _sese_expansion_replacement( self, sese: SESERegion, extracted_bbs: List[ int ] ) -> None :
        function: Function = sese.function
        cfg = function.cfg

        for bb in extracted_bbs :
            entry_node = cfg.graph.predecessors( bb ).__next__()
            exit_node = cfg.graph.successors( bb ).__next__()

            bb_sese = SESERegion( SESERegion.get_new_num(), (entry_node, bb), function )
            bb_sese.exit = (bb, exit_node)
            bb_sese.nodes = { bb }
            bb_sese.to_adapt = False

            function.pdst.graph.add_edge( sese, bb_sese )

    def sese_expansion( self, function: Function ) :
        for sese in filter( lambda s : s.is_region(), list( function.pdst.__iter__() ) ) :
            extracted_bbs = self._sese_expansion_extraction( sese )
            self._sese_expansion_replacement( sese, extracted_bbs )

    def _adapt_sese_check_loopable( self, sese: SESERegion ) -> bool :
        function: Function = sese.function
        program: Program = function.program
        cfg = function.get_cfg()

        analysis_name: str = "CFG_%s" % program.name

        loop_results = self.aiTprojects[ analysis_name ].results[ analysis_name ][ 'xml' ].results[ 'loop_analysis' ]
        if function.name not in loop_results :
            self.logger.warning( """ %s does not contains loop, forcing this region %s to be uncomputable """ % (
                function.name, str( sese )) )
            return False

        loop_name: Optional[ str ] = None
        for loop, loop_data in loop_results[ function.name ][ 'loops' ].items() :
            if int( loop_data[ 'start' ], base=16 ) == cfg.bbs.BBs[ sese.get_entry_edge()[ 1 ] ].start :
                self.logger.info( """ Found a loop start for sese %s : %s""" % (str( sese ), loop) )
                loop_name = loop
                break

        if loop_name is not None :
            loop_data = loop_results[ function.name ][ 'loops' ][ loop_name ]
            start_loop = int( loop_data[ "start" ], base=16 )
            exits = loop_data[ "exit_address" ]

            if len( exits ) != 1 :
                self.logger.warning( """ Removing sese %s : unable to make it loopable """ % str( sese ) )
                return False

            exit_bb_num = sese.get_exit_edge()[ 1 ]
            if type( exit_bb_num ) == int and int( exits[ 0 ], base=16 ) == cfg.bbs.BBs[ exit_bb_num ].start :
                if start_loop == cfg.bbs.BBs[ sese.get_entry_edge()[ 1 ] ].start :
                    sese.loop = int( loop_results[ function.name ][ 'loops' ][ loop_name ][ 'start' ], base=16 )
                    return True

        return False

    def _adapt_sese_removal_pdst_changes( self, domain, sese ) :
        pdst: nx.DiGraph = domain.function.pdst.graph
        ipdst: nx.DiGraph = domain.function.program.ipdst.graph

        domain_predecessors = list( pdst.predecessors( domain ) )
        if len( domain_predecessors ) != 1 :
            raise Exception( " domain has no predecessor " )

        upper_region = domain_predecessors[ 0 ]

        pdst.remove_edge( domain, sese )
        pdst.add_edge( upper_region, sese )

        ipdst.remove_edge( domain, sese )
        ipdst.add_edge( upper_region, sese )

    def _adapt_sese_removal( self, problematic_seses: Dict[ str, List[ SESERegion ] ] ) :
        for key, seses in problematic_seses.items() :
            for sese in seses :
                if not self._adapt_sese_check_loopable( sese ) :
                    self.logger.info( """ Removing %s %s for aiT handling reason """ % (key, str( sese )) )
                    sese.computable = False

                pdst = sese.function.get_pdst().get_graph()
                predecessors_list = list( pdst.predecessors( sese ) )

                if len( predecessors_list ) != 1 :
                    raise Exception( """len(predecessors_list) != 1 in a tree graph for non root node """ )

                predecessor = predecessors_list[ 0 ]

                if predecessor.is_domain() :
                    if sese not in predecessor.seses :
                        raise Exception( """ Sese subSese of domain without being in the domain SESE """ )

                    if sese == predecessor.seses[ 0 ] and key in [ 'mese', 'meme' ] :
                        """
                        Multiple entry at the entry of domain, aiT cannot handle it, so we must remove it
                        In that case, we just need to remove the sese from the domain sese for the domain to adjust itself
                        """
                        predecessor.hard_remove_sese( sese )
                        self._adapt_sese_removal_pdst_changes( predecessor, sese )
                        self.logger.warning(
                            """Hard removing of sese %s from domain %s """ % (str( sese ), str( predecessor )) )
                        if predecessor.is_removable() :
                            """ The domain does not survive the removal so we tag it as not to be processed """
                            predecessor.doNotProcess = True

                    elif sese == predecessor.seses[ -1 ] and key in [ 'seme', 'meme' ] :
                        """
                        Multiple exit at the exit of domain, aiT cannot handle it, so we must remove it
                        In that case, we just need to remove the sese from the domain sese for the domain to adjust itself
                        """

                        predecessor.hard_remove_sese( sese )
                        self._adapt_sese_removal_pdst_changes( predecessor, sese )
                        self.logger.warning(
                            """Hard removing of sese %s from domain %s """ % (str( sese ), str( predecessor )) )
                        if predecessor.is_removable() :
                            """ The domain does not survive the removal so we tag it as not to be processed """
                            predecessor.doNotProcess = True

    def _adapt_seses( self, program: Program ) :
        """
        Remove some sese regions that are not well-handled by aiT.
        These seses regions have the particularity of having multiple out edges on there exit node
        or multiple in edges on there entry node. This case can arise when the other edges form a loop
        inside the sese.
        """

        self.logger.info( """ Adapting the seses to aiT requirements for program %s """ % str( program.name ) )

        """
        Performs the loop analysis for future verification of loop status
        """

        """ Detect all badly-handled seses and store them """
        for function in secured_function_iter( program ) :
            """ Contains all the badly-handled seses by aiT """
            problematic_seses: Dict[ str, List[ SESERegion ] ] = { 'mese' : [ ], 'seme' : [ ], 'meme' : [ ] }

            cfg = function.cfg.get_graph()

            for sese in filter( lambda x : x.is_region(), function.get_pdst() ) :
                if sese.to_adapt :
                    entry_edge = sese.get_entry_edge()
                    exit_edge = sese.get_exit_edge()

                    entry_node = entry_edge[ 1 ]
                    exit_node = exit_edge[ 0 ]

                    out_degree = cfg.out_degree( exit_node )
                    in_degree = cfg.in_degree( entry_node )

                    if out_degree >= 2 and in_degree == 1 :
                        problematic_seses[ 'seme' ].append( sese )

                    elif out_degree == 1 and in_degree >= 2 :
                        problematic_seses[ 'mese' ].append( sese )

                    elif out_degree >= 2 and in_degree >= 2 :
                        problematic_seses[ 'meme' ].append( sese )

            self._adapt_sese_removal( problematic_seses )

    def _gen_new_ait_project( self, name: str ) -> aiTProject :
        if self.aiTprojects.get( name ) is not None :
            self.logger.warning( """ Rewriting the following aiT project : %s""" % name )

        ait_project = aiTProject( name, self.project.tmp_dir.joinpath( name ) )

        self.aiTprojects[ name ] = ait_project
        return ait_project

    def _remove_ait_project( self, ait_project: aiTProject ) -> None :
        name = ait_project.name
        if self.aiTprojects.get( name ) is None :
            self.logger.warning( """ Unable to remove the following aiT project : %s """ % ait_project.name )
            return None

        self.aiTprojects.pop( name )

    def _perform_ait_cfg_analysis( self, program: Program, analysis_name: str ) :
        """
        :param program: Program
        :param analysis_name: str
        """
        cfg_ait_project = self._gen_new_ait_project( analysis_name )

        cfg_ait_project.apx.add_executable( program.binaryPath )

        cfg_analysis = aiTCfgAnalysis( analysis_name )
        cfg_analysis.start = program.starting_point

        cfg_ait_project.add_analysis( cfg_analysis )

        cfg_ait_project.run()

        xml_result = AiTXmlReader.read_xml( cfg_analysis.get_report_path( 'xml' ) )
        cfg_ait_project.results[ analysis_name ][ 'xml' ] = xml_result

        xml_result.loop_extraction()
        xml_result.loop_analysis()

    def _write_generic_ais( self ) :
        """
            Generate a generic AIS header used by all analyses on a program
            This AIS header contains :
                * The starting and ending address of each sese
                * The evaluate instruction for each sese
                * The include to the independent header of each program
        """
        self.genericAisCounter += 1

        for P in self.project.programs :
            ais_file: AisFile = AisFile( self.project.tmp_dir.absolute(),
                                         "seseHeader_%s_%d" % (P.name, self.genericAisCounter) )

            """ Generate a try block where we place all the annotations """
            sese_header_global_mapping = AisMapping( "inf", 2 )
            ais_file.add_instruction( sese_header_global_mapping )

            sese_header_try_zone = AisTryZone()
            ais_file.add_instruction( sese_header_try_zone )

            for function in secured_function_iter( P ) :
                for sese in filter( lambda x : x.is_processable(), function.get_pdst() ) :
                    metadata = sese.metadata

                    """ Adds the information about the sese """
                    sese_header_try_zone.add_instruction(
                        AisLabel( "start_%s" % metadata.id, hex( metadata.startAdd ) ) )
                    sese_header_try_zone.add_instruction( AisLabel( "end_%s" % metadata.id, hex( metadata.endAdd ) ) )

                    if sese.loop is None :
                        sese_header_try_zone.add_instruction(
                            AisEvaluateZone( '"start_%s"' % metadata.id, '"end_%s"' % metadata.id, metadata.id ) )

            """ Includes the independence header of the program """
            ais_file.add_instruction( AisInclude( P.labelIndependantHeader.path.absolute() ) )
            ais_file.add_instruction( AisSuppress( 1031 ) )  # Suppress messages of unfounded label and regions
            ais_file.add_instruction( AisSuppress( 5130 ) )  # Suppress messages of regions with 0 cycles execution time
            ais_file.write()

            self.aisSeseHeaders[ P ] = ais_file
            self.updateBase = False

    def _compute_seses_generate_project( self, project_name: str,
                                         seses: Optional[ List[ Union[ SESERegion, SESEDomain ] ] ],
                                         program: Program ) :
        def gen_evaluating_seses( sese_list, program_evaluating_sese ) :
            if sese_list is not None :
                for s in sese_list :
                    yield s
            else :
                for secured_function in filter( is_secured, program_evaluating_sese.icfg ) :
                    for processable_sese in filter( lambda sese_process : sese_process.is_processable(),
                                                    secured_function.get_pdst() ) :
                        yield processable_sese

        def first_encounter_condition( sese_first_encounter: Union[ SESERegion, SESEDomain ] ) :
            return sese_first_encounter.is_processable() and sese_first_encounter.selected

        sese_header_path = self.aisSeseHeaders[ program ].path

        ait_project = self._gen_new_ait_project( project_name )
        ait_project.apx.add_executable( program.binaryPath )

        for sese in gen_evaluating_seses( seses, program ) :
            self.MITCall += 1
            metadata = sese.metadata

            """ Analysis of the WCET OOC of the sese """
            analysis_name: str = metadata.id
            sese_analysis = aiTWcetAnalysis( analysis_name )
            ait_project.add_analysis( sese_analysis )

            sese_analysis.start = "start"
            sese_analysis.gen_ais( analysis_name )
            sese_analysis.ais.add_instruction( AisInclude( sese_header_path ) )
            sese_analysis.ais.add_instruction( AisLabel( "start", hex( metadata.startAdd ) ) )

            if not metadata.hitTheEnd :
                for end in metadata.stopDecodingAdds :
                    sese_analysis.ais.add_instruction( AisUnknown( "end: %s;\n" % hex( end ) ) )

            for function in dfs_conditional_stop( program.icfg.get_graph(),
                                                  sese.function,
                                                  is_secured,
                                                  lambda x : not is_secured( x ) ) :
                pdst = function.get_pdst()

                source = pdst.get_root()
                if sese.function == function :
                    source = sese

                sub_encounter = lambda s : s != sese and first_encounter_condition( s )

                first_encountered_seses = dfs_conditional_stop( pdst.get_graph(),
                                                                source,
                                                                sub_encounter,
                                                                sub_encounter )

                for sub_sese in first_encountered_seses :
                    # No need to check for usable domain as they already have been removed
                    if sub_sese.loop is None :
                        sese_analysis.ais.add_instruction(
                            AisSnippetNotAnalyzed( "\"start_%s\"" % sub_sese.metadata.id,
                                                   "\"end_%s\"" % sub_sese.metadata.id )
                        )
                    else :
                        sese_analysis.ais.add_instruction(
                            AisLoopTiming( hex( sub_sese.metadata.startAdd ), 0 )
                        )

        return ait_project

    def _compute_seses_recover_results( self, ait_project, program: Program ) :
        prefix = ait_project.name

        for id_analysis, analysis in ait_project.apx.analyses.items() :
            sese = self.seseMapping[ program ][ id_analysis ]

            xml_result = AiTXmlReader.read_xml( analysis.get_report_path( 'xml' ) )
            ait_project.results[ id_analysis ][ 'xml' ] = xml_result
            xml_result.wcet_region_analysis()
            wcet_results = xml_result.results[ 'wcet_region_analysis' ]
            loop_sub_wcet = wcet_results[ "loops" ]
            region_sub_wcet = wcet_results[ "regions" ]

            sese_wcet = xml_result.results[ 'wcet_region_analysis' ][ "global" ]

            if getattr( sese, "WCET_OOC", None ) is None or sese.WCET_OOC >= sese_wcet :
                sese.WCET_OOC = sese_wcet
                sese.WCET_OOC_update = prefix

            sese.subSeseWCET = dict()

            for sub_sese in filter( lambda s : s.is_processable(), program.ipdst.get_descendant( sese ) ) :
                sub_sese_id = str( sub_sese )

                if sub_sese.loop is not None :
                    timing = loop_sub_wcet.get( hex( sub_sese.metadata.startAdd ) )

                else :
                    timing = region_sub_wcet.get( sub_sese_id )

                if timing is not None :
                    sese.subSeseWCET[ sub_sese ] = timing

    def compute_seses( self, prefix: str, seses: Optional[ List[ SesePart ] ] = None ) :
        """
        Run aiT to compute the time of the seses
        """

        if self.updateBase :
            self._write_generic_ais()

        programs = [ seses[ 0 ].function.program ] if seses is not None else self.project.programs
        """
            Generate a new project for each program that will compute the WCET of each sese
            as well as the time taken by each sub-sese
        """
        for program in programs :
            ait_project = self._compute_seses_generate_project( prefix + "%s" % program.name, seses, program )

            """ Execute the project by aiT """
            self.logger.info( """ Running aiT %s, it may take a while """ % ait_project.name )

            start_time = time.time()
            ait_project.run()
            elapsed_time = time.time() - start_time
            self.timeMIT += elapsed_time

            """ Recover the results and place all the data in appropriate structures """
            self._compute_seses_recover_results( ait_project, program )
            self._remove_ait_project( ait_project )

    @classmethod
    def recover_sese_metadata( cls, sese: Union[SESEDomain, SESERegion] ) -> None :
        """ Analyse the sese region and the cfg to decide where to place the end point and other sese feature for aiT
        """
        """
            AiT special cases:
                * At the entry of a function, aiT does not accept to have a not analyzed
                region beginning on the first instruction of a function. However, it does
                accept a "routine snippet takes 0 cycles".

                * At the end of a region, we do not analyze the branch, aiT might complain by detecting strange paths
                * At the end of a region, we need to apply a stop to the analysis at the next instruction after the end of the region when we only
                want to analyse the timing the region.
                * At the end of a region, we must not include the delay-slot inside the bounds of the region, as aiT treat them by duplicating them for each path.
        """

        function = sese.function
        hit_the_end = None

        """ Differentiate between domain and region for the id and the entry and exit edges """
        id_sese = str( sese )
        entry_edge = sese.get_entry_edge()
        exit_edge = sese.get_exit_edge()

        if entry_edge is None or exit_edge is None :
            cls.logger.warning( """ Stop computing sese %s metadata : one edge is None """ % str( sese ) )

        start_bb_num = entry_edge[ 1 ]
        end_bb_num = exit_edge[ 0 ]
        start_bb = function.cfg.bbs.BBs[ start_bb_num ]
        end_bb = function.cfg.bbs.BBs[ end_bb_num ]
        start_add = start_bb.start
        stop_decoding_add = [ ]
        end_add = None
        forward = False

        if exit_edge[ 1 ] == 'end' :
            hit_the_end = True

        if sese.loop is not None :
            if type( exit_edge[ 1 ] ) == int :
                end_add = function.cfg.bbs.BBs[ exit_edge[ 1 ] ].start
                stop_decoding_add.append( end_add )
                forward = True

        else :
            if len( end_bb ) == 1 :
                add = end_bb.end
                inst = function.get_instruction_by_add( add )

                if inst.get_type() in [ 'branch', 'call', 'jump', 'ret' ] :
                    raise Exception(
                        " Exit block of sese contains only one instruction which is a control-flow instruction " )

                end_add = add
                stop_decoding_add.append( add + 0x4 )

            else :
                # Possibility of BB with only 1 CF instruction (ba, call, ret)
                inst = function.get_instruction_by_add( end_bb.end - 0x4 )

                if inst.get_type() in [ "call", "ret" ] :
                    # The next instruction after a call is the instruction following
                    end_add = end_bb.end
                    stop_decoding_add.append( end_add + 0x4 )

                elif inst.get_type() in [ "branch", "jump" ] :
                    pdst = sese.function.pdst
                    start_nodes = set()
                    if sese.is_region():
                        start_nodes = sese.nodes
                    inter_sese_nodes = list( reduce( lambda s1, s2 : s1.union( s2 ), map( lambda s : s.nodes, filter(
                        lambda s : s.is_region(), pdst.get_full_children( sese, lambda s : True ) ) ), start_nodes ) )

                    next_bbs_num = [ num for num in function.cfg.get_graph().successors( end_bb_num ) if num not in inter_sese_nodes ]

                    if len( next_bbs_num ) >= 2 :
                        # Should not happen under aiT but here as a safe net
                        # raise Exception( "computing metadata for sese not handled by aiT" )
                        end_add = end_bb.end - 0x4
                    else :
                        # A single branched instruction (ba, ...)
                        # While aiT can sometime handle the delay-slot inside (when the arriving bb does not have other
                        # edges, ...) it often fails so we take the conservative way
                        end_add = end_bb.end - 0x4

                    for next_bb in map( lambda num : function.cfg.bbs.BBs[ num ], next_bbs_num ) :
                        # Check it is not a part of the region

                        stop_decoding_add.append( next_bb.start )

                else :
                    end_add = end_bb.end
                    stop_decoding_add.append( end_add + 0x4 )

        sese.metadata = SeseAisMetadata( startAdd=start_add, endAdd=end_add, id=id_sese,
                                         stopDecodingAdds=stop_decoding_add,
                                         hitTheEnd=hit_the_end, forward=forward )
