# -*- coding: utf-8 -*-

import pathlib
import pymid.utils.loggingConf


class ApxFile( object ) :
    logger = pymid.utils.loggingConf.getLogger( "ApxFile" )

    def __init__( self, name, path: pathlib.Path, pedantic_level=2 ) :
        self.name = name + ".apx"
        self.path = path.joinpath( self.name )
        self.analyses = { }
        self.executables = []
        self.pedantic_level = pedantic_level
        self.sram_access_sycles = 4

    def add_analysis( self, analysis ) :
        name = analysis.name

        if name in self.analyses :
            self.logger.warning( """ The following analysis \"%s\" already exist and will be overwritten """ % name )

        self.analyses[name] = analysis

    def add_executable( self, exe: pathlib.Path ) :
        self.executables.append( exe.absolute() )

    def gen_file( self ) :
        data = self._gen_header( 0 )
        data += self._gen_executable_files( 1 )
        data += self._gen_analysis( 1 )
        data += self._gen_options( 1 )
        data += self._gen_footer( 0 )

        if self.path.exists() :
            self.logger.warning(
                """ The following apx file already exists and will be overwritten : %s """ % str(
                    self.path.absolute() ) )

        with open( str( self.path.absolute() ), 'w' ) as f :
            f.write( data )

    @classmethod
    def _gen_indent( cls, indent: int = 0 ) -> str:
        return "  "*indent

    @classmethod
    def _gen_header( cls, indent: int = 0 ) -> str:
        header = cls._gen_indent( indent ) + """<!DOCTYPE APX>\n"""
        header += cls._gen_indent( indent ) + """<project xmlns="http://www.absint.com/apx" build="3381670" xsi:noNamespaceSchemaLocation="http://www.absint.com/dtd/a3-apx-18.10.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" target="leon3" version="18.10">" build="3381670" xsi:noNamespaceSchemaLocation="http://www.absint.com/dtd/a3-apx-18.10.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" target="leon3" version="18.10">\n"""
        return header

    def _gen_executable_files( self, indent: int = 0 ) -> str:
        data = self._gen_indent( indent ) + """<files xmlns="http://www.absint.com/apx">\n"""

        indent += 1
        data += self._gen_indent( indent ) + """<executables xmlns="http://www.absint.com/apx">"""

        for i in range( len( self.executables ) ) :
            if i != 0 :
                data += ','
            data += str( self.executables[i] )

        data += """</executables>\n"""

        indent -= 1
        data += self._gen_indent( indent ) + """</files>\n"""

        return data

    def _gen_analysis( self, indent: int = 0 ) -> str:
        data = self._gen_indent( indent ) + """<analyses xmlns="http://www.absint.com/apx">\n"""

        indent += 1

        for analyze in self.analyses.values() :
            data += analyze.write( indent )

        indent -= 1

        data += self._gen_indent( indent ) + """</analyses>\n"""
        return data

    def _gen_options( self, indent=0 ) :
        data = self._gen_indent( indent ) + """<options xmlns="http://www.absint.com/apx">\n"""
        indent += 1
        data += self._gen_indent( indent ) + """<analyses_options xmlns="http://www.absint.com/apx">\n"""
        indent += 1

        data += self._gen_indent( indent ) + """<report_file_verbosity_level xmlns="http://www.absint.com/apx">3</report_file_verbosity_level>\n"""
        data += self._gen_indent( indent ) + """<xml_report_file_verbosity_level xmlns="http://www.absint.com/apx">3</xml_report_file_verbosity_level>\n"""
        data += self._gen_indent( indent ) + """<pedantic_level xmlns="http://www.absint.com/apx">%d</pedantic_level>\n""" % self.pedantic_level
        data += self._gen_indent( indent ) + """<xml_call_graph xmlns="http://www.absint.com/apx">true</xml_call_graph>\n"""
        data += self._gen_indent( indent ) + """<xml_show_per_context_info xmlns="http://www.absint.com/apx">true</xml_show_per_context_info>\n"""
        data += self._gen_indent( indent ) + """<xml_wcet_path xmlns="http://www.absint.com/apx">true</xml_wcet_path>\n"""
        data += self._gen_indent( indent ) + """<xml_non_wcet_cycles xmlns="http://www.absint.com/apx">true</xml_non_wcet_cycles>\n"""
        data += self._gen_indent( indent ) + """<include_output_from_intermediate_decoding_rounds xmlns="http://www.absint.com/apx">true</include_output_from_intermediate_decoding_rounds>\n"""

        indent -= 1
        data += self._gen_indent( indent ) + """</analyses_options>\n"""
        data += self._gen_indent( indent ) + """<general_options xmlns="http://www.absint.com/apx">\n"""

        indent += 1
        data += self._gen_indent( indent ) + """<include_path xmlns="http://www.absint.com/apx">..</include_path>\n"""

        indent -= 1
        data += self._gen_indent( indent ) + """</general_options>\n"""
        data += self._gen_indent( indent ) + """<leon3_options>\n"""

        indent += 1
        data += self._gen_indent( indent ) + """<instruction_cache>\n"""

        indent += 1
        data += self._gen_indent( indent ) + """<enabled>false</enabled>\n"""

        indent -= 1
        data += self._gen_indent( indent ) + """</instruction_cache>\n"""
        data += self._gen_indent( indent ) + """<data_cache>\n"""

        indent += 1
        data += self._gen_indent( indent ) + """<enabled>false</enabled>\n"""

        indent -= 1
        data += self._gen_indent( indent ) + """</data_cache>\n"""
        data += self._gen_indent( indent ) + """<stack_pointer>0x80000000</stack_pointer>\n"""
        data += self._gen_indent( indent ) + """<stack_area>0x60000000 .. 0x80000000</stack_area>\n"""
        data += self._gen_indent( indent ) + """<sram_memory>\n"""
        data += self._gen_indent( indent + 1 ) + """<access_cycles>%d</access_cycles>\n""" % self.sram_access_sycles
        data += self._gen_indent( indent ) + """</sram_memory>\n"""

        indent -= 1
        data += self._gen_indent( indent ) + """</leon3_options>\n"""

        indent -= 1
        data += self._gen_indent( indent ) + """</options>\n"""

        return data

    @classmethod
    def _gen_footer( cls, indent: int = 0 ) -> str:
        return cls._gen_indent( indent ) + """</project>"""
