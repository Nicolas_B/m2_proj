# -*- coding: utf-8 -*-

import pathlib
from pymid.utils import loggingConf


class AisAnnotation( object ) :
    @classmethod
    def write_indent( cls, indent=0 ) :
        return "  " * indent


class AisLoop( AisAnnotation ) :
    def __init__( self, label="", lower=0, upper="inf" ) :
        self.label = label
        self.address = None
        self.lower = lower
        self.upper = upper

    def write_with_label( self, indent=0 ) :
        return self.write_indent( indent ) + "loop \"%s\" { bound: %s .. %s; }\n" % (
        self.label, str( self.lower ), str( self.upper ))

    def write_with_address( self, indent=0 ) :
        return self.write_indent( indent ) + "loop %s { bound: %s .. %s; }\n" % (
            hex( self.address ), str( self.lower ), str( self.upper ))

    def write( self, indent=0 ) :
        if self.address is not None :
            return self.write_with_address( indent )
        else :
            return self.write_with_label( indent )


class AisSnippetNotAnalyzed( AisAnnotation ) :
    def __init__( self, start, continue_at, name="start" ) :
        self.start = start
        self.continueAt = continue_at
        self.name = name

    def write( self, indent=0 ) :
        data = self.write_indent( indent ) \
               + "routine \"%s\" snippet %s to %s takes: 0 cycles;\n" \
               % (self.name,
                  str( self.start ),
                  str( self.continueAt )
                  )
        # data = "  "*indent + "instruction %s snippet {\n" % str(self.start)
        #
        # indent += 1
        # data += "  "*indent + "continue at: %s;\n" % str(self.continueAt)
        # data += "  "*indent + "not analyzed;\n"
        # data += "  "*indent + "takes: 0 cycles;\n"
        #
        # indent -= 1
        # data += "  "*indent + "}\n"

        return data


class AisRecursion( AisAnnotation ) :
    def __init__( self, label="", lower=0, upper="inf" ) :
        self.label = label
        self.lower = lower
        self.upper = upper

    def write( self, indent=0 ) :
        return self.write_indent( indent ) + "routine \"%s\" { recursion bound: %s .. %s; }\n" % (
            self.label, str( self.lower ), str( self.upper ))


class AisTryZone( AisAnnotation ) :
    def __init__( self ) :
        self.insideInst = list()

    def add_instruction( self, inst ) :
        self.insideInst.append( inst )

    def write( self, indent=0 ) :
        s = self.write_indent( indent ) + "try {\n"
        for inst in self.insideInst :
            s += inst.write( indent + 1 )
        s += self.write_indent( indent ) + "}\n"

        return s


class AisEvaluateZone( AisAnnotation ) :
    def __init__( self, start, end, name, inclusive=True ) :
        self.start = start
        self.end = end
        self.name = name
        self.inclusive = inclusive

    def write( self, indent=0 ) :
        if self.inclusive :
            return self.write_indent( indent ) + "evaluate %s to %s inclusive as : \"%s\";\n" % (
                str( self.start ), str( self.end ), self.name)
        else :
            return self.write_indent( indent ) + "evaluate %s to %s exclusive as : \"%s\";\n" % (
                str( self.start ), str( self.end ), self.name)


class AisLabel( AisAnnotation ) :
    def __init__( self, name, value ) :
        self.name = name
        self.value = value

    def write( self, indent=0 ) :
        return self.write_indent( indent ) + "label \"%s\": %s;\n" % (self.name, self.value)


class AisDefine( AisAnnotation ) :
    def __init__( self, name, value ) :
        self.name = name
        self.value = value

    def write( self, indent=0 ) :
        return self.write_indent( indent ) + "define \"%s\": %s;\n" % (self.name, self.value)


class AisInclude( AisAnnotation ) :
    def __init__( self, path: pathlib.Path ) :
        self.path = path.absolute()

    def write( self, indent=0 ) :
        return self.write_indent( indent ) + "include '%s';\n" % (str( self.path ))


class AisSuppress( AisAnnotation ) :
    def __init__( self, message_id ) :
        self.message_id = message_id

    def write( self, indent=0 ) :
        return self.write_indent( indent ) + "suppress message: %s;\n" % (str( self.message_id ))


class AisMapping( AisAnnotation ) :
    def __init__( self, call_string_length="inf", unroll=2 ) :
        self.call_string_length = call_string_length
        self.unroll = unroll

    def write( self, indent=0 ) -> str :
        annotation: str = self.write_indent( indent ) + "mapping {\n"

        indent += 1
        annotation += self.write_indent( indent ) + "max length: %s;\n" % str( self.call_string_length )
        annotation += self.write_indent( indent ) + "default unroll: %s;\n" % str( self.unroll )

        indent -= 1
        annotation += "}\n"

        return annotation


class AisLoopTiming( AisAnnotation ) :
    def __init__( self, loop_name: str, timing: int ) :
        self.loop_name: str = loop_name
        self.timing: int = timing

    def write( self, indent=0 ) :
        return self.write_indent( indent ) + "loop %s takes : %d cycles;\n" % (self.loop_name, self.timing)


class AisUnknown( AisAnnotation ) :
    def __init__( self, unknown ) :
        self.unknown = unknown

    def write( self, indent=0 ) :
        return self.write_indent( indent ) + self.unknown


class AisFile( object ) :
    logger = loggingConf.getLogger( "AisFile" )

    def __init__( self, path: pathlib.Path = pathlib.Path( '.' ), name="", to_write=True ) :
        self.name = name + '.ais'
        self.path = path.joinpath( self.name ).absolute()
        self.data = list()
        self.to_write=to_write

    def add_instruction( self, inst ) :
        self.data.append( inst )

    def write( self ) :
        if self.to_write:
            data = ""

            for d in self.data :
                data += d.write( indent=0 )

            if self.path.exists() :
                self.logger.warning(
                    """ The following ais file already exists and will be overwritten : %s """ % str( self.path ) )

            with open( str( self.path ), 'w' ) as f :
                f.write( data )
