# -*- coding: utf-8 -*-
import pathlib

from pymid.aiT.ais import AisFile
from pathlib import Path


class aiTAnalysis( object ) :
    def __init__( self, name, xml_report="", html_report="", txt_report="", gdl_report="" ) :
        self.name = name
        self.start = None
        self.xml_report = xml_report
        self.html_report = html_report
        self.txt_report = txt_report
        self.gdl_report = gdl_report
        self.path = Path( name )
        self.ais = None
        self.type = None

    def write( self, indent=0 ) :
        if self.path is None or self.start is None or (
                self.xml_report == "" and self.html_report == "" and self.txt_report == "" and self.gdl_report == "") or self.type is None :
            raise Exception( """Unable to write the following analysis because some data are missing""", self )

        if self.ais is not None :
            self.ais.write()

        data = "  " * indent + """<analysis xmlns="http://www.absint.com/apx" type=\"%s\" enabled=\"true\" id=\"%s\">\n""" % (
            self.type, self.name)

        indent += 1
        data += "  " * indent + """<analysis_start xmlns="http://www.absint.com/apx">%s</analysis_start>\n""" % self.start

        if self.ais is not None :
            data += "  " * indent + """<ais xmlns="http://www.absint.com/apx">%s</ais>\n""" % str( self.ais.path )
        if self.xml_report != "" :
            data += "  " * indent + """<xml_report xmlns="http://www.absint.com/apx">%s</xml_report>\n""" % self.path.joinpath(
                self.xml_report ).absolute()
        if self.html_report != "" :
            data += "  " * indent + """<html_report xmlns="http://www.absint.com/apx">%s</html_report>\n""" % self.path.joinpath(
                self.html_report ).absolute()
        if self.txt_report != "" :
            data += "  " * indent + """<report xmlns="http://www.absint.com/apx">%s</report>\n""" % self.path.joinpath(
                self.txt_report ).absolute()
        if self.gdl_report != "" :
            data += "  " * indent + """<gdl xmlns="http://www.absint.com/apx">%s</gdl>\n""" % self.path.joinpath(
                self.gdl_report ).absolute()

        indent -= 1
        data += "  " * indent + """</analysis>\n"""

        return data

    def gen_ais( self, name : str ) -> AisFile:
        self.ais = AisFile( self.path, name )
        return self.ais

    def add_ais( self, ais: AisFile ):
        if type(ais) != AisFile:
            raise Exception(" Adding an ais that is not a AisFile ")

        self.ais = ais

    def get_report_path( self, report : str) -> pathlib.Path:
        if report == "xml" and self.xml_report != "" :
            return self.path.joinpath( self.xml_report ).absolute()

        elif report == "html" and self.html_report != "" :
            return self.path.joinpath( self.html_report ).absolute()

        elif report == "txt" and self.txt_report != "" :
            return self.path.joinpath( self.txt_report ).absolute()

        elif report == "gdl" and self.gdl_report != "" :
            return self.path.joinpath( self.gdl_report ).absolute()

        else :
            raise Exception( "Unable to parse the ask report type" )


class aiTCfgAnalysis( aiTAnalysis ) :
    def __init__( self, name ) :
        super().__init__( name, xml_report="%s.xml" % name )
        self.type = 'control_flow_graph'

    def write( self, indent=0 ) :
        return super().write( indent )


class aiTWcetAnalysis( aiTAnalysis ) :
    def __init__( self, name ) :
        super().__init__( name, xml_report="%s.xml" % name )
        self.type = 'wcet_analysis'

    def write( self, indent=0 ) :
        return super().write( indent )
