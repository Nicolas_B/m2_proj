# -*- coding: utf-8 -*-

import collections
import pymid.utils.loggingConf


class aiTXml( object ) :
    logger = pymid.utils.loggingConf.getLogger( "aiTXml" )
    ns = { 'a3_report' : 'http://www.absint.com/a3report' }
    aiTBB = collections.namedtuple( 'aiTBB', [ 'firstInstruction', 'lastInstruction', 'loopClass' ] )

    def __init__( self, raw_xml, name: str = "" ) :
        self.raw_xml = raw_xml
        self.name = name
        self.results = dict()

    def wcet_region_analysis( self ) :
        """
        Analyse the WCET result of each regions specified to aiT
        """
        self.results[ 'wcet_region_analysis' ] = {
            "regions" : { },
            "routines" : { },
            "loops" : { },
            "global" : None
        }

        root = self.raw_xml.getroot()

        wcet_regions = root.findall( './/a3_report:wcet_analysis/a3_report:wcet_results/a3_report:wcet_region[@name]',
                                     aiTXml.ns )

        wcet_routines = root.findall( './/a3_report:wcet_analysis/a3_report:wcet_results/a3_report:wcet_routine',
                                      aiTXml.ns )

        entry_name = root.find( ".//a3_report:wcet_analysis/a3_report:wcet_results", aiTXml.ns ).get( "entry" )
        entry_results_node = root.find( ".//a3_report:wcet_analysis//a3_report:wcet_routine[@routine=\"%s\"]" % entry_name, aiTXml.ns )
        entry_results = entry_results_node.get( "cumulative_cycles" )

        for region in wcet_regions :
            name = region.attrib[ 'name' ]
            wcet = region.attrib[ 'cumulative_cycles' ]

            self.results[ 'wcet_region_analysis' ][ "regions" ][ name ] = int( wcet )

        for routine in wcet_routines :
            id_routine = routine.attrib[ 'routine' ]
            wcet = routine.attrib[ 'cumulative_cycles' ]

            routine_node = root.find( './/a3_report:cfg_value_analysis//a3_report:routine[@id=\"%s\"]' % id_routine,
                                      aiTXml.ns )
            routine_address = routine_node.get( "address" )

            if routine_node.get( "loop" ) is None :
                self.results[ 'wcet_region_analysis' ][ "routines" ][ routine_address ] = int( wcet )
            else :
                self.results[ 'wcet_region_analysis' ][ "loops" ][ routine_address ] = int( wcet )

        self.results[ 'wcet_region_analysis' ][ "global" ] = int( entry_results )

    def loop_bound_analysis( self ) :
        """
        Extract the loop bound from aiT
        """
        self.results[ 'loop_bound_analysis' ] = { }

        root = self.raw_xml.getroot()
        routines = \
            root.findall( './/a3_report:wcet_analysis_task/a3_report:cfg_value_analysis/a3_report:routines',
                          aiTXml.ns )[ 0 ]
        loopbounds = \
            root.findall( './/a3_report:wcet_analysis_task/a3_report:cfg_value_analysis/a3_report:loopbounds',
                          aiTXml.ns )[ 0 ]

        r_map = dict()
        bounds = dict()

        for r in routines :
            attrib = r.attrib
            r_map[ attrib[ "id" ] ] = attrib[ 'name' ]

        for loopbound in loopbounds :
            attr = loopbound.attrib
            routine_id = attr[ 'routine' ]

            if routine_id in bounds :
                bounds[ routine_id ][ 'min' ] = min( bounds[ routine_id ][ 'min' ], int( attr[ 'min' ] ) )
                bounds[ routine_id ][ 'max' ] = max( bounds[ routine_id ][ 'max' ], int( attr[ 'max' ] ) )

            else :
                bounds[ routine_id ] = dict()
                bounds[ routine_id ][ 'min' ] = int( attr[ 'min' ] )
                bounds[ routine_id ][ 'max' ] = int( attr[ 'max' ] )

        for r_id, r in bounds.items() :
            r_name = r_map[ r_id ]
            self.results[ 'loop_bound_analysis' ][ r_name ] = (r[ 'min' ], r[ 'max' ])

    def loop_extraction( self ) :
        """
        Extract the loops and basic information on them
        """
        analysis_name: str = 'loop_extraction'
        if analysis_name in self.results :
            self.logger.warning(
                "The following analysis have already been performed but will be overwritten : %s " % analysis_name )

        self.results[ analysis_name ] = dict()

        root = self.raw_xml.getroot()
        routines = root.findall( './/a3_report:cfg_value_analysis/a3_report:routines', aiTXml.ns )[ 0 ]

        for r in routines :
            attributes = r.attrib
            if attributes.get( 'loop' ) :
                # This is a loop, we retrieve the basic information
                name = attributes[ 'name' ]
                extracted_from = attributes[ 'extracted_from' ]
                start = attributes[ 'address' ]

                self.results[ analysis_name ][ name ] = dict()
                self.results[ analysis_name ][ name ][ 'extracted_from' ] = extracted_from
                self.results[ analysis_name ][ name ][ 'start' ] = start

    def loop_analysis( self ) :
        analysis_name = 'loop_analysis'
        if analysis_name in self.results :
            self.logger.warning(
                "The following analysis have already been performed but will be overwritten : %s " % analysis_name )

        self.results[ analysis_name ] = { }

        root = self.raw_xml.getroot()
        routines = root.findall( './/a3_report:cfg_value_analysis/a3_report:routines', aiTXml.ns )[ 0 ]

        # The first loop allows to save for each function the loops inside the function and for each routine the ids of the blocks

        for r in routines :
            attributes = r.attrib
            if attributes.get( 'loop' ) :
                # This is a loop, we retrieve the basic information
                name = attributes[ 'name' ]
                extracted_from = attributes[ 'extracted_from' ]
                start = attributes[ 'address' ]
                loop_id = attributes[ "id" ]

                self.results[ analysis_name ].setdefault( extracted_from, { } ).setdefault( 'loops', { } ).setdefault(
                    name, { } )
                self.results[ analysis_name ][ extracted_from ][ 'loops' ][ name ][ 'start' ] = start
                self.results[ analysis_name ][ extracted_from ][ 'loops' ][ name ][ 'id' ] = loop_id
                self.results[ analysis_name ][ extracted_from ][ 'loops' ][ name ][ 'block ids' ] = set()
                self.results[ analysis_name ][ extracted_from ][ 'loops' ][ name ][ 'basic blocks' ] = set()

                for block in r :
                    bId = block.attrib[ 'id' ]
                    self.results[ analysis_name ][ extracted_from ][ 'loops' ][ name ][ 'block ids' ].add( bId )

            else :
                name = attributes[ 'name' ]
                start = attributes[ 'address' ]

                self.results[ analysis_name ].setdefault( name, { } )[ 'start' ] = start
                self.results[ analysis_name ][ name ][ 'block ids' ] = set()

                for block in r :
                    bId = block.attrib[ 'id' ]
                    self.results[ analysis_name ][ name ][ 'block ids' ].add( bId )

        for r in routines :
            attributes = r.attrib

            if attributes.get( 'loop' ) is not None :
                self._analyze_one_loop( r )

    def _analyze_one_loop( self, loop ) :
        analysis_name = 'loop_analysis'

        name = loop.attrib[ 'name' ]
        extracted_from = loop.attrib[ 'extracted_from' ]

        root = self.raw_xml.getroot()
        calling_routine = root.find(
            ".//a3_report:cfg_value_analysis/a3_report:routines/a3_report:routine[@name='%s']" % extracted_from,
            aiTXml.ns )

        # Find all the "instruction blocks" i.e. the blocks that contains instruction addresses
        instruction_blocks = loop.findall( './/a3_report:first_instruction/..', aiTXml.ns )

        # Find the call blocks that can be interesting to find loop nesting
        call_successors = loop.findall( "./a3_report:block[@type='call']/a3_report:successor", aiTXml.ns )
        end_successors = loop.findall( "./a3_report:block[@type='end']/a3_report:successor", aiTXml.ns )

        for block in instruction_blocks :
            first_instruction = block.find( './a3_report:first_instruction', aiTXml.ns ).text
            last_instruction = block.find( './a3_report:last_instruction', aiTXml.ns ).text
            loop_class = block.find( './a3_report:loop_classification', aiTXml.ns ).text

            bb = aiTXml.aiTBB( first_instruction, last_instruction, loop_class )

            self.results[ analysis_name ][ extracted_from ][ 'loops' ][ name ][ 'basic blocks' ].add( bb )

        for successor in call_successors :
            bId = successor.text

            """
            4 possibilities :
            * bId is a block of the loop itself -> nothing interesting
            * bId is in the function blocks -> we found an exit node, not that interesting
            * bId is in another function -> we found a real 'call' instruction that we probably already have ...
            * bId is in another loop of the same function -> we found a nested loop
            
            TODO : Refactore using find method
            """

            found_block = False

            # First possibility
            for blockId in self.results[ analysis_name ][ extracted_from ][ 'loops' ][ name ][ 'block ids' ] :
                if blockId == bId :
                    found_block = True
                    break

            if found_block :
                continue

            # Second possibility
            for blockId in self.results[ analysis_name ][ extracted_from ][ 'block ids' ] :
                if blockId == bId :
                    found_block = True
                    break

            if found_block :
                continue

            # Fourth possibility (we won't check the 3rd possibility ^^ )
            for otherLoop in self.results[ analysis_name ][ extracted_from ][ 'loops' ] :
                if found_block :
                    break

                if otherLoop == name :
                    continue

                for blockId in self.results[ analysis_name ][ extracted_from ][ 'loops' ][ otherLoop ][ 'block ids' ] :
                    if blockId == bId :
                        self.results[ analysis_name ][ extracted_from ][ 'loops' ][ name ].setdefault( 'innerLoop',
                                                                                                       [ ] ).append(
                            otherLoop )
                        self.results[ analysis_name ][ extracted_from ][ 'loops' ][ otherLoop ].setdefault( 'outerLoop',
                                                                                                            [ ] ).append(
                            name )
                        found_block = True
                        break

            if found_block :
                continue

            # raise Exception(""" Found a loop block successor of aiT result with no matching """)

        for successor in end_successors :
            bId = successor.text
            routine_containing_block = root.find( ".//a3_report:block[@id='%s']/.." % str( bId ), aiTXml.ns )

            if routine_containing_block != loop :
                for exit_block_id in map( lambda b : b.text,
                                          root.findall( ".//a3_report:block[@id='%s']/a3_report:successor" % str( bId ),
                                                        aiTXml.ns ) ) :
                    exit_block = root.find( ".//a3_report:block[@id='%s']" % str( exit_block_id ), aiTXml.ns )
                    exit_address = exit_block.get( "address" )
                    self.results[ analysis_name ][ extracted_from ][ 'loops' ][ name ].setdefault( 'exit_address',
                                                                                                   [ ] ).append(
                        exit_address )
