# -*- coding: utf-8 -*-

import pathlib
import re
import xml.etree.ElementTree as ET

import pymid.aiT.ais
from pymid.aiT.xmlAnalyses.aiTXml import aiTXml
import pymid.utils.loggingConf


class AisReader :
    logger = pymid.utils.loggingConf.getLogger( "AisReader" )

    def __init__( self ) :
        """ Virtually private constructor """
        raise Exception( "The class AisReader only contains static method" )

    @staticmethod
    def analyze_ais( path: pathlib.Path ) :
        ais_path = str( path.absolute() )

        raw_data = []
        ais_data = pymid.aiT.ais.AisFile( path.absolute() )

        # Read the file and suppress the comments
        with open( ais_path, 'r' ) as file :
            for line in file :
                stripped_line = line.strip()

                if len( stripped_line ) > 0 and stripped_line[0] != "#" :
                    raw_data.append( stripped_line )
        """
        For now, we only treat annotation of the type :
            loop <label> { bound: <int> .. <int>; }
            routine <label> { recursion bound: <int> .. <int>; }

        All other annotations are stripped
        Furthermore, we require that no space are introduce in label names and to respect the exact spacing of the previous annotations
        """

        loop_regex = re.compile(
            'loop "(?P<label>[^"]*)" { bound: (?P<lower>((0x)|0|b)?[0-9a-fA-F]*) .. (?P<upper>((0x)|0|b)?[0-9a-fA-F]*); }' )
        recursion_regex = re.compile(
            'routine "(?P<label>[^"]*)" { recursion bound: (?P<lower>((0x)|0|b)?[0-9a-fA-F]*) .. (?P<upper>((0x)|0|b)?[0-9a-fA-F]*); }' )

        for line in raw_data :
            m = loop_regex.match( line )
            if m is not None :
                loop = pymid.aiT.ais.AisLoop( m.group( 'label' ), m.group( 'lower' ), m.group( 'upper' ) )
                ais_data.add_instruction( loop )
                continue

            m = recursion_regex.match( line )
            if m is not None :
                rec = pymid.aiT.ais.AisRecursion( m.group( 'label' ), m.group( 'lower' ), m.group( 'upper' ) )
                ais_data.add_instruction( rec )
                continue

            AisReader.logger.warning( """Ignoring line : %s""" % str( line ) )

            ais_data.add_instruction( pymid.aiT.ais.AisUnknown( line ) )

        return ais_data


class AiTXmlReader( object ) :
    logger = pymid.utils.loggingConf.getLogger( "AiTXmlReader" )

    def __init__( self ) :
        """ Virtually private constructor """
        raise Exception( "The class AiTXmlReader only contains static method" )

    @staticmethod
    def read_xml( path: pathlib.Path ) :
        xml_path = str( path.absolute() )

        raw_xml = ET.parse( xml_path )

        return aiTXml( raw_xml, xml_path )
