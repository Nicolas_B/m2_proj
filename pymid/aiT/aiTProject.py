# -*- coding: utf-8 -*-

import pathlib
import subprocess

import pymid.aiT.Apx
from pymid.aiT.ais import AisFile


class aiTProject( object ) :
    def __init__( self, name, path: pathlib.Path, pedantic_level=2 ) :
        self.name = name
        self.path = path
        self.apx = pymid.aiT.Apx.ApxFile( name, path.absolute(), pedantic_level )
        self.ais = { }
        self.results = { }  # Store the different result analysis per aiTAnalysis

        self.path.mkdir( parents=True, exist_ok=True )

    def add_analysis( self, analysis ) :
        analysis_name = analysis.name

        analysis_path = self.path.joinpath( analysis_name ).absolute()

        analysis_path.mkdir( parents=True, exist_ok=True )

        analysis.path = analysis_path

        self.apx.add_analysis( analysis )

        self.results[analysis_name] = dict()

    def gen_ais( self, name ) :
        new_ais = AisFile( self.path, name )
        self.ais[name] = new_ais

        return new_ais

    def run( self ) :
        self.apx.gen_file()

        for aisF in self.ais.values() :
            aisF.write()

        p = subprocess.run( ['alauncher', '-b', '-j', '-1', str( self.apx.path.absolute() )], stdout=subprocess.PIPE )

        if p.returncode != 0 :
            raise Exception( "AiT call failed : %s" % self.name )

    def __str__( self ) :
        return "< AiTProject %s >" % self.name

    def __repr__( self ) :
        return self.__str__()
